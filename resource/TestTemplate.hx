package ::PackageName::;

import ::PackageName::.::ClassName::;

import azunit.ITestCase;
import azunit.TestStatus;
import azunit.Assert;

/**
 * 
 * Built with AzUnit v::LibVersion::
 * 
 * @author {Author}
 */
class ::ClassName::Test implements ITestCase<::ClassName::>
{
	private var instance : ::ClassName::;
	
	function setup()
	{
		
	}	
	::if Methods::
	::foreach Methods::
	@Test function ::__current__::Test():Void
	{
		
	}
	::end::
	::else::
	@Test function testSomething():Void
	{
		Assert.isTrue(false);
	}
	::end::	
	
}