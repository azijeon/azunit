/**
 * ...
 * @author Аз
 */
(function() 
{
	var $search = $("#search");
	
	console.log("$search: ", $search);
	
	$search.keyup(function()
	{
		$(".test-result").show();
		//$(".test-result:not([data-class*='"+ $search.val() +"'])").hide();
		
		var val = $search.val();
		
		if(val && val.length > 0)
		{
			$(".test-result:not([data-class*='"+ val +"'])").hide();
		}
		
		if(history && history.pushState)
		{
			history.pushState(null, null, '#' + $search.val());
		}
		
	});
	
	if(window.location.hash.length)
	{
		$search.val(window.location.hash.replace("#", ""));
		$search.keyup();
	}
	
})();