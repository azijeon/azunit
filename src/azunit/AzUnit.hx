package azunit;

import azunit.Compare;
import azunit.macro.AssertMeta;
import azunit.macro.Config;
import azunit.macro.SuitBuilder;
import haxe.Json;
import haxe.macro.Context;
import haxe.macro.Expr.ExprOf;

#if macro
import haxe.macro.Compiler;
import sys.io.File;
import haxe.io.Path;
import sys.FileSystem;
#end

using StringTools;

typedef MacroType = haxe.macro.Type;

/**
 * ...
 * @author Az
 */
@:keep()
class AzUnit
{
	/**
	 * Returns the AzUnit library version currently in use
	 * @return
	 */
	macro public static function version():ExprOf<String>
	{
		var paths 	= Context.getClassPath();
		var result 	= "?.?.?";
		
		var lib_path:String = Path.join([libDir(), "haxelib.json"]);
		
		// Read lib json
		var file = File.read(lib_path);
		var json = file.readAll().toString();
		
		file.close();
		
		var data = Json.parse(json);
		
		result = data.version;				
		
		
		
		return macro $v { result };
	}
	
	/**
	 * Returns a 
	 * @param	file
	 * @return
	 */
	macro public static function resource(file:String):ExprOf<String>
	{
		var paths = Context.getClassPath();
		var content : String;
		
		var res_file = '${libDir()}/resource/$file';
		
		if (FileSystem.exists(res_file))
		{
			content = File.getContent(res_file);
		}
		
		return macro $v{content};
	}
	
	#if macro
	public static function init()
	{
		//version();
		
		// Load AzUnit configuration
		Config.load();
		
		if(Config.build_suit)
			SuitBuilder.buildSuit(Config.testSrc);
		
		Context.onGenerate(onGenerate);
		Context.onAfterGenerate(onAfterGenerate);
		
		#if azunit
		Compiler.addGlobalMetadata("", "@:build(azunit.macro.GenericBuilder.build())", true, true);
		
			#if cs
			Compiler.addGlobalMetadata("", "@:rtti", true, true);
			#end
		#end
	}
	
	private static function onGenerate(types:Array<MacroType>)
	{
		// Save the assert info
		AssertMeta.save();
	}
	
	private static function onAfterGenerate()
	{
		
	}
	
	/**
	 * Returns the path to the AzUnit directory
	 * macro only
	 * @return
	 */
	private static function libDir():String
	{
		var paths 	= Context.getClassPath();
		var exp		= new EReg(".azunit(.src.?)?$", "gi");
		
		for (cp in paths)
		{
			if (exp.match(cp))
			{
				return Path.join([cp, ".."]);				
			}
		}
		
		return null;
	}
	#end
}