package azunit;
import azijeon.azlib.exception.Exception;
import azijeon.azlib.exception.InvalidArgument;
import haxe.ds.Either;

/**
 * A helper used to compare values
 * 
 * @author Az
 */
class Compare
{
	/**
	 * Check if a value is between two other given values
	 * 
	 * @param	a
	 * @param	b
	 * @param	val
	 * @param	not_inclusive
	 * @return
	 */
	public static function between(min:Dynamic, max:Dynamic, val:Dynamic, not_inclusive:Bool = false):Bool
	{
		if (not_inclusive)
		{
			return (val > min && val < max);
		}
		else
		{
			return (val >= min && val <= max);
		}
	}
	
	/**
	 * Checks if two variables are exactly equal to each other
	 * 
	 * performs deep check
	 * 
	 * @param	a
	 * @param	b
	 * @return
	 */
	public static function equals(a:Dynamic, b:Dynamic):Bool
	{
		// Compare two arrays
		if (isArray(a) && isArray(b))
		{
			return equalsArray(a,b);
		}
		
		// Compare two objects
		if (isObject(a) && isObject(b))
		{
			return equalsObject(a, b);
		}
	
		// Compare two anything else
		return a == b;
	}
	
	/**
	 * Returns true if a value is in given iterable
	 * 
	 * @param	val
	 * @param	array
	 * @return
	 */
	public static function isIn(val:Dynamic, array:Dynamic):Bool
	{
		var it = iterator(array);
		
		if (it == null)
			throw new Exception('Value "$array" is not iterable!');
		
		for (item in it)
		{
			if (equals(val, item))
				return true;
		}
		
		return false;
	}
	
	private static function equalsObject(a:Dynamic, b:Dynamic):Bool
	{
		var keys = Reflect.fields(a);
		
		for (key in keys)
		{
			if (!Reflect.hasField(b, key))
				return false;
				
			if (!equals(Reflect.field(a, key), Reflect.field(b, key)))
				return false;
		}
		
		return true;
	}
	
	
	
	private static function equalsArray(a:Array<Dynamic>, b:Array<Dynamic>):Bool
	{
		
		if (a.length != b.length)
			return false;
		
		for (i in 0...a.length)
		{
			//trace('${a[i]} == ${b[i]}');
			
			if (!equals(a[i], b[i]))
				return false;
		}
		
		return true;
	}
	
	private static function isObject(a:Dynamic):Bool
	{
		return Reflect.isObject(a) && !Std.isOfType(a, String) && !Std.isOfType(a, Float) && !Std.isOfType(a, Bool);
	}
	
	private static inline function isArray(a:Dynamic):Bool
	{
		return Std.isOfType(a, Array);
	}
	
	public static function count(a:Dynamic):Null<Int>
	{
		var iterator : Iterator<Dynamic> = iterator(a);
		
		if (iterator != null)
		{
			var count = 0;
			
			while (iterator.hasNext())
			{
				iterator.next();
				count++;
			}
			
			return count;
		}
		else
		{
			throw new InvalidArgument("Value is not countable! - " + Assert.dump(a));
		}
		
		return null;
	}
	
	private static function iterator<T>(a:Dynamic):Iterator<T>
	{
		if (#if !js Reflect.hasField(a, "iterator") && #end Reflect.isFunction(a.iterator))
		{
			return Reflect.callMethod(a, a.iterator, []);
		}
		#if js
		else if (Reflect.isFunction(a.hasNext) && Reflect.isFunction(a.next))
		{
			return a;
		}
		#else
		else if (Reflect.hasField(a, "hasNext") && Reflect.hasField(a, "next") && Reflect.isFunction(a.hasNext) && Reflect.isFunction(a.next))
		{
			return a;
		}
		#end
		else
		{
			return null;
		}
	}
	
	
}