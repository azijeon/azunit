package azunit.mock;
import azijeon.azlib.data.CountMap;

/**
 * @author Az
 */
#if !macro
//@:autoBuild(azunit.mock.AzMockWIP.build())
#end
interface IMocked 
{
	private var azunit_mock_call_count:CountMap;
}