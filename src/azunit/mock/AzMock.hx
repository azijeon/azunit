package azunit.mock;

import azijeon.azlib.macro.TypeTool;
import azijeon.azlib.tool.ArrayTools;
import azunit.macro.MacroTools;
import azunit.mock.IMocked;
import haxe.DynamicAccess;
import haxe.Exception;
import haxe.ds.StringMap;
import haxe.macro.Expr;
import haxe.macro.Expr.ExprOf;
import haxe.macro.Context;
import haxe.macro.ExprTools;
import haxe.macro.Type;
import haxe.ds.StringMap;

using azijeon.azlib.tool.ArrayTools;

typedef CountMap = azijeon.azlib.data.CountMap;

/**
 * ...
 * @author 
 */
class AzMock 
{
	static var mock_index : CountMap = new CountMap();
	
	macro public static function count(fn:ExprOf<Function>):Expr
	{
		//trace(fn);
		
		switch(fn.expr)
		{
			case EField(e, field) : 
				return macro { @:privateAccess callCount(${e}, $v{field}); }
			
			default: null;
		}
		
		return macro null;
	}
	
	private static function callCount(obj:IMocked, name:String):Int
	{
		return @:privateAccess obj.azunit_mock_call_count.get(name);
	}
	
	macro public static function mock(cls:ExprOf<Class<Dynamic>>, ?data:Null<Expr>):Expr
	{
		var cls_name = MacroTools.resolveTypeName(cls);
		var pack = cls_name.split(".");
		var name = pack.pop();
		
		var index = mock_index.add(cls_name);
		
		var mock_data = parseData(data);
		var mock_name = '${name}Mock' + index;
		
		var c = Context.getType(cls_name);
		
		var cls : ClassType;
		var cls_params : Array<Type>;
		
		switch (c)
		{
			case TInst(t, p) :  
				cls = t.get();
				cls_params = p;
			
			default: throw new Exception("Only classes and interfaces can be mocked. For now...");
		}
		
		var mock 	: TypeDefinition = {
			name 	: mock_name,
			pack 	: pack,
			fields 	: getMockFields(cls, mock_data),
			kind 	: buildKind(cls), 
			
			pos: Context.currentPos(),
		}
		
		Context.defineType(mock);
		
		var mock_full = pack.join(".") + "." + mock_name;
		//var mock_type : TypePath = {
			//name : mock_name,
			//pack : pack,
			//params: cls.params.map(mapParams),
		//}
		
		return macro AzMock.createInstance($i{mock_name});
	}
	
	public static function createInstance<T:IMocked>(cls:Class<T>):T
	{
		var inst : T = Type.createEmptyInstance(cls);
		@:privateAccess cast(inst, IMocked).azunit_mock_call_count = new CountMap();
			
		return inst;
	}	
	#if macro
	
	
	private static function buildKind(cls:ClassType):TypeDefKind
	{
		var type = {
			name 	: cls.name,
			pack 	: cls.pack, 
			params 	: cls.params.map(mapParams),					
		};
		
		var interfaces = cls.interfaces.map(function(i):TypePath
		{
			var cls = i.t.get();
			
			return {
				name 	: cls.name,
				pack 	: cls.pack,
				params 	: cls.params.map(mapParams), 
			}
		});
		
		var imocked = Context.getType("azunit.mock.IMocked");
		
		switch (imocked)
		{
			case TInst(t, p) : interfaces.push({
				name: t.get().name,
				pack: t.get().pack,
				params : [],
			});			
			default: null;
		}
		
		if (cls.isInterface)
		{
			interfaces.push(type);
			type = null;
		}
		
		return TDClass(type, interfaces, false, false, false);
	}
	
	private static function mapParams(p:TypeParameter):TypeParam
	{
		switch (p.t)
		{
			case TInst(t, params) :
				
				var type = t.get();
				
				//trace('${type.pack.join(".")}.${type.name} -- ${type.kind}');
				
				// Map all generic parameters to dynamic and hope for the best
				return TPType(TPath({
						name : "Any",
						pack : [],
						params : [],
					}));
					
				//return TPType(TPath({
					//name: type.name,
					//pack: type.pack,
					//params: type.params.map(mapParams),
				//}));
				
				
			default: throw new Exception("Should be TInst, but it's not. Weird");
		}
				
	}
	
	private static function getMockFields(cls:ClassType, data:DynamicAccess<Expr>):Array<Field>
	{
		var fields = cls.fields.get().map(function(f:ClassField):Field
		{
			switch (f.kind)
			{
				case FMethod(k) : return mockMethod(f, k, cls, data);
				default: return null;
			}
		})
		.filter(function(v) return v != null);
		
		//fields.push({
			//name: "new",
			//access: [APublic],
			//kind: FFun({
				//args: [],
				//expr: macro { super(null); },
			//}),
			//pos: Context.currentPos(),
			//
		//});
		
		fields.push({
			name: "azunit_mock_call_count",
			access: [APublic],
			kind: FVar(macro : azijeon.azlib.data.CountMap, null),
			pos: Context.currentPos(),
		});
		
		return fields;
	}
	
	private static function mockMethod(f:ClassField, k:MethodKind, cls:ClassType, data:DynamicAccess<Expr>):Field
	{
		var access = [];
		access.push(f.isPublic ? APublic : APrivate);
		
		if (cls.isInterface == false)
			access.push(AOverride);
		
		var args;
		var ret;
		
		f.params = f.params.map(function(p)
		{
			return {
				name : p.name,
				t : Context.getType("Any"),
			}
		});
		
		switch (f.type)
		{
			case TFun(a, r) :
					args = a;
					ret = r;
					
			case TLazy(lf) :				
				switch (lf())
				{
					case TFun(a, r) : 							
						args = a;
						ret = r;
						
					default:
						throw new Exception('Hmm. Should have been TFun: ${f.name}');
				}
				default: 
					throw new Exception('Hmm. Should have been TFun: ${f.name}');
		}
		
		var expr : Expr;
		
		switch (ret)
		{
			case TAbstract(t, p) :
				if (Std.string(t) == "Void")
				{
					expr = macro { ${mockedMethodExpr(f, data)}};					
				}
				
			default: null;
		}
		
		if (expr == null)
		{
			expr = macro {
				${mockedMethodExpr(f, data)}
				return null; 
				
			};
		}
		
		//trace(ret.getParameters());
		//trace(f.params.find(function(p) 
		//{
			//trace('${p.t} -- $ret');
			//return p.t.equals(ret);
		//}));
		
		var field = 
		{
			name 	: f.name,
			access 	: access,
			kind	: FFun({
				ret  : toComplexType(ret),
				args : args.map(function(a):FunctionArg
				{
					return {
						name: a.name,
						opt: a.opt,
					}
				}),
				expr : expr,
			}), 
			pos: Context.currentPos(),
		}
		
		return field;
	}
	
	private static function toComplexType(src:Type):ComplexType
	{
		switch (src)
		{
			case TInst(t, params) :
				
				if (params.length > 0)
				{
					return TPath({
						name : t.get().name,
						pack : t.get().pack,
						params : params.map(function(p)
						{
							return TPType(TPath({
								name : "Any",
								pack : [],
								params : [],
							}));
						})
					});
				}
				
			default: null;
		}
		
		return Context.toComplexType(src);
	}
	
	private static function mockedMethodExpr(f:ClassField, data:DynamicAccess<Expr>):Expr
	{
		var count_exp = macro 
		{
			// Count the number of times the method is called
			cast(this, azunit.mock.IMocked).azunit_mock_call_count.add($v{f.name});
		}
		
		// If mock implementation exists
		if (data.exists(f.name))
		{
			var e = data.get(f.name);
			
			var source_args;
			var source_ret;
			
			switch (f.type)
			{
				case TFun(a, r) :
					source_args = a;
					source_ret = r;
				
				case TLazy(lf) :
					switch (lf())
					{
						case TFun(a, r) :
							source_args = a;
							source_ret = r;
							
						default: throw new Exception("Hmm. Should have been TFun");
					}
					
				
				default: throw new Exception("Hmm. Should have been TFun");
			}
			
			switch (e.expr)
			{
				case EFunction(kind, fn) :
					
					// TODO - Add argument check
					
					return macro {
						${count_exp};
						${fn.expr};
					};
					
				default: throw new Exception("Function definition expected!");
			}
			
			return e;
		}
		
		return count_exp;
		
	}
	
	private static function parseData(d:Expr):DynamicAccess<Expr>
	{
		switch (d.expr)
		{
			case EObjectDecl(fields) :
				
				var data = new DynamicAccess();
				
				for (f in fields)
				{
					data.set(f.field, f.expr);
					
					//trace('${f.field} - ${f.expr}');
				}
				
				return data;
				
			case EConst(c) :
				if(ExprTools.getValue(d) == null)
					return {};
				else
					throw new Exception('Unexpected $d');
				
				
			default: 
				return {};
				//throw new Exception("Object declaration expected");
		}
	}	
	#end
}