package azunit.mock;

import haxe.DynamicAccess;
import haxe.macro.Expr;
import haxe.macro.Expr.ExprOf;
import haxe.macro.Compiler;
import haxe.macro.Context;
import haxe.macro.Expr.TypeDefinition;
import haxe.macro.Type.ClassField;
import haxe.macro.Type.ClassType;

import azunit.macro.MacroTools;
import haxe.macro.Type;
using haxe.macro.Tools;

/**
 * Azijeon mocking prototype not based on Mockatoo WIP
 * 
 * @author Az
 */
class AzMockWIP 
{
	static var id_seed : Int = 0;
	
	static var stat_field : String = "azijeon_azunit_stat_data";
	static var mock_map : DynamicAccess<TypePath> = {};
	
	macro public static function mock(cls:ExprOf<Class<Dynamic>>, ?impl_exp:Expr):Expr
	{
		var mock : TypeDefinition  = null;
		
		var cls_name : String = MacroTools.resolveTypeName(cls);
			
		var t = Context.getType(cls_name).follow();
		var c = t.getClass(); 
		var p = cls.pos;
		
		var imp : DynamicAccess<Expr> = {};
		
		if (impl_exp != null)
			imp = parseImplementation(impl_exp);
		
		mock = createMock(c, imp);
		
		Context.defineType(mock);
		
		return Context.parse('new ${mock.pack.join(".")}.${mock.name}()', Context.currentPos());
		
	}
	
	#if macro
	/**
	 * Creates a mock from ClassType 
	 * 
	 * TODO - Figure out how to support generic params
	 * 
	 * @param	c
	 * @param	imp_map
	 * @return
	 */
	private static function createMock(c:ClassType, ?imp_map:DynamicAccess<Expr>):TypeDefinition
	{
		trace('Mocking: ${c.pack.concat([c.name]).join(".")} ${c.params}');
		
		var superClass  : TypePath				= null;
		var interfaces  : Array<TypePath> 		= [];
		var params		: Array<TypeParamDecl> 	= mapParams(c.params);
		
		var fields = mockFields(c, imp_map);
		
		// Add the mocked interface
		if (c.isInterface)
			interfaces.push(MacroTools.toTypePath(c.pack.concat([c.name]).join(".")));
			
		// Add the AzMock interface
		interfaces.push(MacroTools.toTypePath(IMocked));
		
		if (c.superClass != null)
			superClass = mockSuper(c);
		
		// Create contructor 
		fields.push(mockConstructor(c));
		
		// Define param types
		for (p in c.params)
		{
			
		}
		
		// Create type definition
		var t : TypeDefinition = 
		{
			pack 	: c.pack,
			params 	: params,
			name 	: 'Mock${c.name}${++id_seed}',
			pos 	: Context.currentPos(), 
			meta 	: null,
			
			fields 	: fields,
			
			kind : TDClass(superClass, interfaces)
		};
		
		return t;
	}
	
	private static function mockSuper(c:ClassType):TypePath
	{
		var sup		: ClassType = c.superClass.t.get();
		var sup_cls : String 	= sup.pack.concat([sup.name]).join(".");
		
		if (!mock_map.exists(sup_cls))
		{
			var t 			= createMock(sup);
			var mock_name 	= t.pack.concat([t.name]).join(".");
			var path		= MacroTools.toTypePath(mock_name);
			
			// Create paths for sup parameters
			path.params = sup.params.map(function(p)
			{
				trace('Param: ${p.name} - ${sup.pack}');
				
				return TPType(TPath({
					name 	: p.name,
					pack 	: sup.pack.concat([sup.name]), 
					params 	: [],					
				}));	
			})
			.filter(function(p) return p != null);
			
			Context.defineType(t);
			
			mock_map.set(sup_cls, path);
		}
		
		return mock_map.get(sup_cls);
	}
	
	private static function mockConstructor(c:ClassType):Field
	{
		var args : Array<FunctionArg> 	= [];
		var expr : Expr 				= null;
		
		if (c.constructor != null)
		{
			var con 	= c.constructor.get();
			var type 	= null;
			
			switch(con.type)
			{
				case TLazy(lazy) 	: type = lazy();
				default 			: type = con.type; 
			}
			
			// Extract constructor args
			switch(type)
			{
				case TFun(a, r) :
					
					args = mapArgs(a).map(function(a)
					{
						// Make all args optional
						a.opt = true;
						return a;
					});					
					
				default: throw 'Invalid constructor type: ${type}';
			}
		}
		
		// Define constructor expression
		if (c.isInterface || c.superClass == null)
		{
			expr = macro { this.$stat_field = cast {}; };
		}
		else
		{
			var call_args 	= [for(a in args) macro null];
			expr 			= macro { super($a{call_args}); this.$stat_field = {} };
		}
		
		var construct = {
			name 	: "new",
			access 	: [APublic],
			pos 	: Context.currentPos(),
			
			kind : FFun({
				args : args,
				ret  : null,
				expr : expr,
			})			
		}
		
		return construct;
	}
	
	
	private static function mockFields(c:ClassType, imp_map:DynamicAccess<Expr>):Array<Field>
	{
		var out = [];
		
		for (f in c.fields.get())
		{
			switch(getClassFiledType(f))
			{
				// Mock method
				case TFun(_, _) : out.push(mockField(f, imp_map));
				
				// Do not mock ANYthing else
				default: continue;
			}
			
		}
		
		return out;
	}
	
	/**
	 * Create mock for single field of ClassType
	 * 
	 * TODO - add public property support
	 * 
	 * @param	f
	 * @return
	 */
	private static function mockField(f:ClassField, imp:DynamicAccess<Expr>):Field
	{
		var access = (f.isPublic ? APublic : APrivate);
		var kind 	: FieldType = null;
		var is_void : Bool 		= false;
		var expr 	: Expr 		= null;
		var type	: Type 		= null;
		
		// Log stat expression
		var log_stats = macro {
			var name 	= $v{f.name}; 
			var stats	= azunit.mock.AzMockWIP.stats(this, name);
			
			stats.call_count++;
		}
		
		
		var type = getClassFiledType(f);
		
		
		switch(type)
		{
			// Mock method
			case TFun(args, ret) : 
				
				// Determine if the return type is Void or not
				switch(ret)
				{
					case TAbstract(v, a) : is_void = (Std.string(v) == "Void");
					default: null;
				}
				
				if (imp != null && imp.exists(f.name))
				{
					// If field exits in implementation map, use expression
					var i = imp.get(f.name);
					expr = macro { ${log_stats};  return ${i} };
				}
				else if (is_void)
				{
					// If Void, use default void exp
					expr = macro { ${log_stats}; return; };
				}
				else
				{
					// If not void, use default return null
					expr = macro { ${log_stats}; return null; };
				}
				
				// Create the field function
				kind = FFun({
					args : mapArgs(args),
					ret  : Context.toComplexType(ret),
					expr : expr, 
				});
				
			default: null;
		}
		
		if (kind == null)
			trace('Null kind: ${f.name} - ${f.type}');
		
		return {
			name 	: f.name,
			access 	: [access],
			kind 	: kind,
			
			pos : Context.currentPos(),
		}
	}
	
	private static function getClassFiledType(f:ClassField):Type
	{
		switch(f.type)
		{
			// If type is lazy, obtain proper type
			case TLazy(lazy) 	: return lazy();
			// Else , continue on with the show
			default 			: return f.type; 
		}
	}
	
	/**
	 * Parses the expression for implementation definition into a usable map
	 * 
	 * @param	impl
	 * @return
	 */
	private static function parseImplementation(impl:Expr):DynamicAccess<Expr>
	{
		var out : DynamicAccess<Expr> = {};
		
		if (impl == null || !isNotNull(impl))
			return out;
		
		switch(impl.expr)
		{
			case EObjectDecl(fields) : 
				
				for (f in fields)
				{
					var exp : Expr = null;
					
					switch(f.expr.expr)
					{
						case EFunction(name, fn) : exp = fn.expr;
						case EConst(v)			 : exp = f.expr;
						
						default: continue;
					}
					
					out.set(f.field, exp);
				}
				
			default: throw "Invalid implementation map!";
		}
		
		return out;
	}
	
	/**
	 * Maps arguments from TFun(args, ret) to Array<FunctionArg> for use in field definition
	 * 
	 * @param	args
	 * @return
	 */
	private static function mapArgs(args:Array<{ name : String, opt : Bool, t : Type }>):Array<FunctionArg>
	{
		return args.map(function(a)
		{
			return {
				name 	: a.name,
				opt 	: a.opt,
				meta	: null,
				type 	: Context.toComplexType(a.t),
				value 	: null, 
			};
		});
	}
	
	private static function mapParams(params:Array<TypeParameter>):Array<TypeParamDecl>
	{
		return params.map(function(p)
		{
			var params : Array<TypeParamDecl> = [];
			var name : String = null;
			var constraints = [];
			
			switch(p.t)
			{
				case TInst(c, pa) : 
					switch (c.get().kind)
					{
						case ClassKind.KTypeParameter(constrnts):
							for (const in constrnts)
							{
								var complex = Context.toComplexType(const);
								constraints.push(complex);
							}
						default:
					}
					
				default : Context.warning('Unexpected type ${p.t}', Context.currentPos());
			}
			
			return {
				name 		: p.name,
				constraints : constraints,
				meta 		: null, 
				params		: [],
			}
		});
	}
	
	private static function isNotNull(expr:Expr):Bool
	{
		switch (expr.expr)
		{
			case EConst(c):
				switch (c)
				{
					case CIdent(id):
						if (id == "null") return false;
					default: null;
				}
			default: null;
		}
		return true;
	}
	
	macro public static function build():Array<Field>
	{
		var fields 		= Context.getBuildFields();
		var has_stat	= MacroTools.hasBuildField(stat_field);
		
		
		trace('Building mock: ${Context.getLocalClass().get()}');
		//trace('Building params: ${Context.getLocalClass().get().params[0]}');
		
		for (p in Context.getLocalClass().get().params)
		{
			switch(p.t)
			{
				case TInst(a, b) : trace('--- ${a.get().pack} -- ${a.get().name} -- $b'); //trace(Context.getType(Std.string(a)));
				default: null;
			}
		}
		
		
		if (!has_stat)
		{
			var stat = {};
			
			fields.push({
				name 	: stat_field,
				access 	: [APrivate],
				pos 	: Context.currentPos(),
				kind 	: FVar(macro:Dynamic, null),
			});
		}
		
		return fields;
	}
	
	
	#end
	
	/**
	 * Returns stat data for mocked object method
	 * 
	 * @param	m
	 * @param	name
	 * @return
	 */
	public static function stats(m:IMocked, name:String):StatData
	{
		var stats : DynamicAccess<StatData> = cast Reflect.field(m, stat_field);
		
		if (!stats.exists(name))
		{
			stats.set(name, {
				call_count : 0,
			});
		}
		
		return stats.get(name);
	}	
}