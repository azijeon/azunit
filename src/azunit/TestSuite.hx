package azunit;

import azunit.ITestCase;
import azunit.ITestSuite.SuiteInfo;

/**
 * ...
 * @author Az
 */
class TestSuite implements ITestSuite
{
	var name 	: String;
	var tests 	: Array<Class<ITestCase<Dynamic>>> = [];
	
	public function new(name:String = "") 
	{
		this.name = name;
	}
	
	public function add(test:Class<ITestCase<Dynamic>>):Void
	{
		tests.push(test);
	}
	
	public function iterator()
	{
		return tests.iterator();
	}
	
	public function info():SuiteInfo
	{
		return {name : name };
	}
}
