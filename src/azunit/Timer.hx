package azunit;

import haxe.Timer;
import haxe.ds.IntMap;

/**
 * ...
 * @author Az
 */
class Timer
{
	static var interval_seed 	: Int = 0;
	static var interval_map 	: IntMap<Timer>;
	
	var time 		: Int = 300;
	var timer 		: haxe.Timer;
		
	@:isVar public var run (default, set): Void->Void;
	
	public static function __init__()
	{
		interval_map = new IntMap();
	}
	
	public function new(interval : Int = 300) 
	{
		this.time 	= interval;		
	}
	
	public function set_run(v:Void->Void):Void->Void
	{
		this.run = v;
		
		#if (php)
		untyped __call__("usleep", this.time);
		this.run();
		
		#else
		timer = new haxe.Timer(time);
		timer.run = v;
		#end
		
		return v;
	}
	
	public function stop():Void
	{
		#if (!php && !neko)
		timer.stop();
		#end
	}
	
	/**
	 * Creates a delayed callback
	 * 
	 * @param	cb
	 * @param	time
	 */
	public static function timeout(cb:Void->Void, time:Int)
	{
		var timer = new Timer(time);
		
		timer.run = function()
		{
			timer.stop();
			cb();
		}
	}
	
	public static function interval(cb:Void->Void, time:Int):Int
	{
		var id		= interval_seed++;
		var timer 	= new Timer(time);
		timer.run 	= cb;
		
		interval_map.set(id, timer);
		
		return id;
	}
	
	public static function clearInterval(id:Int):Bool
	{
		return interval_map.remove(id);
	}
}