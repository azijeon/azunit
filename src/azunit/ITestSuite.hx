package azunit;

/**
 * @author Az
 */
interface ITestSuite 
{
	public function iterator():Iterator<Class<ITestCase<Dynamic>>>;
	public function info():SuiteInfo;
}

typedef SuiteInfo = 
{
	name : String,
}