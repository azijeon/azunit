package azunit.output;
import azunit.ITestSuite.SuiteInfo;
import azunit.TestResult;
import azunit.TestStatus;
import haxe.Template;
import haxe.Resource;
import azijeon.azlib.AzLib;

#if macro
import sys.io.File;
import sys.FileSystem;
import haxe.macro.Expr;
import haxe.macro.Context;
#end

using StringTools;

/**
 * ...
 * @author Аз
 */
class HtmlOutput implements IResultOutput
{
	static var template : Template;
	
	var html 		: String;
	var script 		: String;
	
	public function new() 
	{
		if (template == null)
			template = new Template(AzUnit.resource("HtmlOutput/template.html"));
			
		if (script == null)
		{
			script  = AzUnit.resource("HtmlOutput/jquery.js");
			script += AzUnit.resource("HtmlOutput/script.js");
		}
	}
	
	public function output(result:TestResult):String
	{
		html = template.execute({content: writeHTML(result), script: this.script});
		
		#if js
		js.Browser.document.write(html);
		#end
		
		#if php
		azijeon.azlib.php.PHP.echo(html);
		#end
		
		return html;
	}
	
	
	private function writeHTML(result:TestResult):String
	{
		var current_suite : SuiteInfo = null;
		
		var res 	= result.isSuccess() ? "SUCCESS" : "FAILED";
		var s_count = result.successCount();
		var f_count = result.failCount();
		var a_count = result.allCount();
		
		var asert_done_count 	= result.assertsDoneCount();
		var asert_all_count 	= result.assertsAllCount();
		
		var buf : StringBuf = new StringBuf();
		
		buf.add("<p>Test Complete - ");
		buf.add(	'<span class="${res.toLowerCase()}">$res</span>');
		buf.add("</p>");
		
		buf.add('<p>Tests - performed: $a_count, successful: $s_count, failed: $f_count</p>');
		buf.add('<p>Asserts performed: $asert_done_count of $asert_all_count</p>');
		
		writeWarnings(buf, result);
		
		buf.add("<table>");
		
		for (test in result)
		{
			var status = test.isPassed() ? "success" : "failed";
			
			// Suit header
			if (diffSuits(current_suite, test.suite))
			{
				current_suite = test.suite;
				writeSuite(buf, current_suite);
			}
			
			// Write Test
			buf.add('<tr class="test-result ${status}" data-class="${test.className()}" data-method="${test.method}">');
			buf.add(	'<td>');
			buf.add(		'${test.className()} - ${test.method}() ${writeAssertions(test)}');
			buf.add(		writeErrors(test));
			buf.add(	'</td>');
			
			buf.add(	'<td class="status">');
			buf.add(		test.isPassed() ? "OK" : "FAIL");
			buf.add(	'</td>');
			buf.add('</tr>');			
		}
		
		buf.add("</table>");
		
		return buf.toString();
	}
	
	private function writeWarnings(buf:StringBuf, result:TestResult)
	{
		var warnings = result.assertsWarnCount();
		
		if (warnings > 0)
		{
			buf.add('<p class="warning">Assert warnings: $warnings</p>');
		}		
	}
	
	/**
	 * Returns true if two suits are different from one another
	 * @param	one
	 * @param	other
	 * @return
	 */
	private function diffSuits(one:SuiteInfo, other:SuiteInfo):Bool
	{
		if (one == null && other != null)
			return true;
			
		if (one != null && other == null)
			return true;
		
		if (one.name != other.name)
			return true;
			
		return false;
	}
	
	/**
	 * Writes the test suite header
	 * 
	 * @param	buf
	 * @param	test
	 */
	private function writeSuite(buf:StringBuf, suite:SuiteInfo)
	{
		buf.add("<tr>");
		buf.add(	"<td colspan='2'>");
	
		if (suite != null && suite.name.length > 0)
		{
			buf.add("<h5 class='suite-header'>");
			buf.add(suite.name);
			buf.add("</h5>");
		}
		
		buf.add(	"</td>");
		buf.add("</tr>");
	}
	
	private function writeStyle():String
	{
		var out : StringBuf = new StringBuf();
		out.add('<style type="text/css">');
		
		out.add(".azunit table td\n{");
		out.add(	"\n\tpadding: 5px 0;");
		out.add(	"\n\tborder-bottom: 1px dashed black;");
		out.add("\n}\n");
		
		out.add(".azunit .test-result.failed\n{");
		out.add(	"\n color: red;");		
		out.add("\n}\n");
		
		
		out.add("</style>");
		return out.toString();
	}
	
	private function writeAssertions(test:TestStatus):String
	{
		var out = [];
		
		for (a in test.asserts())
		{
			if (!a.is_done)
			{
				out.push("?");
			}
			else if (a.pass)
			{
				out.push(".");
			}
			else
			{
				out.push("x");				
			}
		}
		
		return out.join("");
	}
	
	private function writeErrors(test:TestStatus):String
	{
		var buf 		= new StringBuf();
		var no_assets 	= true;
		
		
		
		for (a in test.asserts())
		{
			if (a.is_done && a.fail)
			{
				var full 	= a.file;
				var short 	= new EReg("[\\/]","").split(a.file).pop();
				buf.add('<li class="error"><span title="$full" class="file">$short:${a.line}</span> - ${a.error}</li>');
			}
			else if (a.is_done && !a.fail && !a.pass)
			{
				// If it is warning
				var full 	= a.file;
				var short 	= new EReg("[\\/]","").split(a.file).pop();
				buf.add('<li class="warning"><span title="$full" class="file">$short:${a.line}</span> - ${a.error}</li>');
			}
			
			no_assets = false;
		}	
		
		if (no_assets)
		{
			buf.add('<li class="error">No asserts</li>');
		}
		
		if (buf.length > 0)
		{
			return "<ul class='errors'>" + buf.toString() + "</ul>";
		}
		
		
		return "";
	}
}