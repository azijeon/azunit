package azunit.output;
import azijeon.azlib.exception.Exception;
import azunit.TestResult;
import azunit.TestStatus;
import haxe.Json;

using StringTools;


/**
 * ...
 * @author Az
 */
class PlainOutput implements IResultOutput
{
	var data : Array<String>;
	
	var indent_count : Int = 0;
	
	public function new() 
	{
		
	}
	
	private function init()
	{
		data = [];
	}
	
	private function write(text:String):Void
	{
		data.push(text);
	}
	
	private function writeLine(?line:String = ""):Void
	{
		printIndent();
		data.push(Std.string(line));
		data.push("\n");
	}
	
	private function padRight(text:String):Void
	{
		write("<{padRight}>");
		write(text);
	}
	
	private function indent():Void
	{
		indent_count++;
	}
	
	private function outdent():Void
	{
		if (indent_count > 1)
			indent_count--;
			
		indent_count++;
	}

	private function printIndent():Void
	{
		for (i in 0...indent_count)
		{
			data.push(" ");
		}
	}
	
	private function writeHRule():Void
	{
		data.push("<{hr}>\n");
	}
	
	private function parseOutput():String
	{
		var lines : Array<String> = data.join("").split("\n");
		
		var line_length = 0;
		
		for (line in lines)
		{
			if (line.length > line_length)
				line_length = line.length;
		}
		
		// Parse Pad
		for (l in 0...lines.length)
		{
			var line = lines[l];
			
			if (line.indexOf("<{padRight}>") >= 0)
			{
				var pair = line.split("<{padRight}>");
				
				var pad = line_length - (pair[0].length + pair[1].length);
				
				if (pad > 0)
				{
					var tail = pair.pop();
					
					for (i in 0...pad)
					{
						pair.push(" ");
					}
					
					pair.push(tail);
				}
				
				lines[l] = pair.join("");
			}
		}
		
		// Print HR
		var hr = [];
		
		for (i in 0...line_length)
		{
			hr.push("-");
		}
		
		var out = lines.join("\n");
		out 	= out.replace("<{hr}>", hr.join(""));
		
		return out;
	}
	
	public function output(result:TestResult):String
	{
		var res 	= result.isSuccess() ? "SUCCESS" : "FAILED";
		var s_count = result.successCount();
		var f_count = result.failCount();
		var a_count = result.allCount();
		
		var asert_done_count 	= result.assertsDoneCount();
		var asert_all_count 	= result.assertsAllCount();
		
		init();
		write('Test Complete');
		padRight(res + "\n\n");
		
		writeLine('Tests - performed: $a_count, successful: $s_count, failed: $f_count');
		writeLine('Asserts performed: $asert_done_count of $asert_all_count');
		
		write("\n");
		writeHRule();
		
		for (t in result)
		{
			printTest(t);
			writeHRule();
		}
		
		return parseOutput();
	}
	
	/**
	 * Print individual test data
	 * 
	 * @param	buf
	 * @param	result
	 */
	private function printTest(test:TestStatus):Void
	{
		var cls = test.className();
		
		write('$cls : ${test.method}()');
		indent();
		
		var errors = new Array<String>();
		
		for (a in test.asserts())
		{
			if (!a.is_done)
			{
				write("?");
			}
			else if (a.pass)
			{
				write(".");
			}
			else
			{
				write("x");
				printError(errors, a);
			}
		}
		
		// If no assertions
		if (test.isFail() && errors.length == 0)
		{
			errors.push("No asserts");
		}
		
		if (test.isPassed())
		{
			padRight("OK");
		}
		else
		{
			padRight("FAIL");
		}
		
		write("\n");
		
		if (errors.length > 0)
		{
			write("\n");
		}
		
		for (e in errors)
		{
			writeLine(e);
		}		
		
		outdent();		
	}
	
	private function printError(buf:Array<String>, a:AssertStatus):Void
	{
		try
		{
			var line = a.line != null ? Std.string(a.line) : "NULL";			
			var msg = '${a.file}:${line} - (${a.assert}) - ${a.error}';
				
			buf.push(msg);		
		}
		catch (e)
		{
			throw new Exception('printError() failed!', null, e);
		}
	}	
}