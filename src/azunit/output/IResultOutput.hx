package azunit.output;
import azunit.TestResult;

/**
 * @author Az
 */
interface IResultOutput
{
	public function output(result:TestResult):String;
}