package azunit;

import azunit.ITestSuite.SuiteInfo;

/**
 * @author Az
 */
@:autoBuild(azunit.macro.TestBuilder.buildTest())
@:keepSub
@:publicFields
interface ITestCase<T>
{
	private var instance 	: T;	
}