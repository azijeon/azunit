package azunit;

import azunit.macro.AssertMeta;
import azunit.ITestSuite;

/**
 * ...
 * @author Az
 */
@:keep()
@:keepSub()
class TestStatus
{
	public var test 	: ITestCase<Dynamic>;
	public var method 	: String;
	public var suite	: SuiteInfo;
	
	var expected_asserts : Array<AssertStatus> = [];
	
	var is_done 	: Bool = false;
		
	public function new(test:ITestCase<Dynamic>, method:String, ?suite:SuiteInfo) 
	{
		this.test 	= test;
		this.method = method;
		this.suite 	= suite;
		
		this.expected_asserts = [];
		
		var key 	= AssertMeta.key(Type.getClassName(Type.getClass(test)), method);
		var info 	= AssertMeta.get(key);
		
		for (i in info)
		{
			expected_asserts.push(cast i);
		}
	}
	
	public function className():String
	{
		return Type.getClassName(Type.getClass(test));
	}
	
	public function asserts():Iterator<AssertStatus>
	{
		return expected_asserts.iterator();
	}
	
	/**
	 * Pass a given assertion
	 * 
	 * @param	info
	 */
	public function pass(assert_id:Int):Void
	{
		var a = getAssertion(assert_id);
		
		// Pass assertion only if it wasnt already failed
		if (a != null && a.fail != true)
		{
			a.pass 		= true;
			a.fail		= false;
			a.is_done 	= true;
		}
	}
	
	/**
	 * Fail a given assertion
	 * 
	 * @param	reason
	 */
	public function fail(assert_id:Null<Int>, reason:String):Void
	{
		var a = getAssertion(assert_id);
		
		if (a != null)
		{
			a.is_done 	= true;
			a.pass		= false;
			a.fail		= true;
			a.error		= reason;
		}
	}
	
	/**
	 * Aborts the test with an error. 
	 * 
	 * (marks as both failed and done)
	 * @param	reason
	 */
	public function exit(reason:String):Void
	{
		this.fail(null, reason);
		this.is_done = true;
	}
	
	/**
	 * End the test with warning. Marks it as done, but neither success nor fail.
	 * 
	 * @param	assert_id
	 * @param	reason
	 */
	public function warn(assert_id:Int, reason:String):Void
	{
		var a = getAssertion(assert_id);
		
		if (a != null)
		{
			a.is_done 	= true;
			a.fail 		= false;
			a.pass		= false;
			a.error		= reason;
		}
	}
	
	public function isDone():Bool
	{
		if (is_done)
			return true;
		
		for (a in expected_asserts)
		{
			if (a.is_done != true)
				return false;
		}
		
		is_done = true;
		return true;
	}
	
	public function isPassed():Bool
	{
		// Fail if there are no asserts
		if (expected_asserts.length == 0)
			return false;
		
		// Fail if at least one assert has failed
		for (a in expected_asserts)
		{
			if (a.fail)
				return false;
		}
		
		return true;
	}
	
	public function isFail():Bool
	{
		// Fail if there are no asserts
		if (expected_asserts.length == 0)
			return true;
		
		// Fail if at least one assert has failed
		for (a in expected_asserts)
		{
			if (a.fail)
				return true;
		}
		
		return false;
	}
	
	
	public function isWarning():Bool
	{
		for (a in expected_asserts)
		{
			// Is not a warning if it is either success or fail
			if (a.fail == true || a.pass == true)
				return false;
		}
		
		// If all asserts are 
		return true;
	}
	
	/**
	 * Returns a local assertion 
	 * 
	 * @param	info
	 * @return
	 */
	private function getAssertion(assert_id:Int):AssertStatus
	{
		var max : Int = 0;
		
		for (i in expected_asserts)
		{
			if (i.id > max)
				max = i.id;
			
			if (i.id == assert_id)
			{
				return i;
			}
		}
		
		// If id is undefined, create assertion on the fly to accomodate it
		var a : AssertStatus = cast { 
			id: (max + 1),
			file : "",
			line : null,
		};
		
		expected_asserts.push(a);
		
		return a;
	}
}

typedef AssertStatus =
{
	> AssertInfo,
	
	@:optional var is_done 	: Bool;
	@:optional var pass 	: Bool;
	@:optional var fail 	: Bool;
	@:optional var error 	: String;
}