package azunit;
import azijeon.azlib.exception.Exception;
import azijeon.azlib.exception.NativeException;
import azunit.ITestSuite.SuiteInfo;
import azunit.output.IResultOutput;
import azunit.output.PlainOutput;
import azunit.output.HtmlOutput;
import haxe.Json;
import haxe.PosInfos;
import haxe.ds.StringMap;
import azunit.input.Args;
import haxe.Log;

import haxe.rtti.Meta;

using StringTools;

/**
 * ...
 * @author Az
 */
class TestRunner
{
	var TIEMOUT : Int = 5 * 1000;
	
	var _tests 				:Array<Class<ITestCase<Dynamic>>>;
	var _result 			:TestResult;
	
	public var result (get, null)	:TestResult;
	
	var output_format : IResultOutput;
	var on_complete : Array<TestRunner->Void> = [];
	
	var suite_order : Array<String> = [];
	
	public static function __init__()
	{
		setupTrace();
	}
	
	public function new() 
	{
		this._tests 			= [];
		this.output_format 		= new PlainOutput();
	}
	
	/**
	 * Makes adjustments to the native trace function for better output
	 */
	private static function setupTrace()
	{
		var native = Log.trace;
		
		#if php
		Log.trace = function(v:Dynamic, ?pos:PosInfos)
		{
			// If PHP, wrap the tace output in <pre> for readablity
			azijeon.azlib.php.PHP.echo("<pre>");
			native(v, pos);
			azijeon.azlib.php.PHP.echo("</pre>");
		}
		
		#end
	}
	
	public function tests():Iterator<Class<ITestCase<Dynamic>>>
	{
		return _tests.iterator();
	}
	
	/**
	 * Register a test to be performed
	 * 
	 * @param	test
	 */
	public function add(test:Class<ITestCase<Dynamic>>):Void
	{
		_tests.push(test);
	}
	
	public function addSuite(suite:ITestSuite):Void
	{
		for (t in suite)
		{
			var info : SuiteInfo = suite.info();
			
			// Perserve the order in which suites are defined
			if (suite_order.indexOf(info.name) < 0)
			{
				suite_order.push(info.name);
			}
			
			Reflect.setField(t, "suite" , info);
			add(t);
		}
	}
	
	public function run():Void
	{
		_result = new TestResult();
		
		var tests = filterTests();
		
		for (test in tests)
		{
			runTest(test);
		}
		
		checkResult();
		
		#if cs
		// On C# - wait for input before exiting unless the --exit parameter is passed
		if (Sys.args().indexOf("--exit") < 0)
		{
			cs.system.Console.WriteLine("Press any key...");
			cs.system.Console.ReadKey();
		}
		#end
	}
	
	private function filterTests():Array<Class<ITestCase<Dynamic>>>
	{
		var out = [];
		
		var group 	: String = Args.get("group");
		var single 	: String = Args.get("single");
		
		for (t in _tests)
		{
			var test_name = Type.getClassName(t);
			
			// Push if single
			if (single != null && matchSingle(test_name, single))
			{
				out.push(t);
			}
			
			// Push if group
			if (group != null)
			{
				
			}
			
			// If no filter provided, push All
			if (single == null && group == null)
			{
				out.push(cast t);
			}
		}
		
		return out;
	}
	
	private function matchSingle(test_name:String, single:String)
	{
		if (single.indexOf(".") <= 0)
		{
			// Match class name only
			return test_name.split(".").pop() == single;
		}
		else
		{
			// Match full path
			return test_name == single;
		}
	}
	
	/**
	 * Execute a test case class
	 * 
	 * @param	test_class
	 */
	private function runTest(test_class:Class<ITestCase<Dynamic>>):Void
	{
		var fields = getTestFields(test_class);
		
		// Call beforeClass
		callIfExists(test_class, "beforeClass");
		
		for (fname in fields)
		{
			// Create instance
			var test 	= Type.createInstance(test_class, []); 
			var field 	= Reflect.field(test, fname);
			var status 	= new TestStatus(test, fname, Reflect.field(test_class, "suite"));
			
			// Inject instance result
			Reflect.setField(test, "azunit_test_result", status);
			
			// Call setup
			callIfExists(test, "setup");
			
			try
			{
				Reflect.callMethod(test, field, [status]);
			}
			catch (e:Dynamic)
			{
				e = Exception.fromNative(e);
				
				trace("Error: " + e);
				var msg1 = Std.string(e);
				
				// TODO - This was necessary for some reason, but as of now it is unclear why ?
				//
				try
				{
					// Try to call without status arg
					Reflect.callMethod(test, field, []);
				}
				catch (e2:Dynamic)
				{
					e2 = Exception.fromNative(e2);
					
					var msg2 = Std.string(e2);
					
					if (msg1 != msg2)
					{
						msg2 += ' \n${msg1}';
						trace("Error:  " + e2);
					}
						
					status.exit(msg2);					
				}
			}
		
			
			// Call tearDown
			callIfExists(test, "tearDown");
			
			// Register Result
			_result.add(status);
		}	
		
		// Call afterClass
		callIfExists(test_class, "afterClass");
	}
	
	/**
	 * Call a method on the test object if it exists
	 * @param	test
	 */
	private function callIfExists(test:Dynamic, method:String, ?args:Array<Dynamic>):Void
	{
		if (args == null)
			args = [];
		
		var method_ref = Reflect.field(test, method);
			
		if (method_ref != null && Reflect.isFunction(method_ref))
		{
			Reflect.callMethod(test, method_ref, []);			
		}
	}
	
	
	private function checkResult():Void
	{
		if (_result.isDone())
		{
			report();			
		}
		#if !php
		else
		{
			// Wait for complete
			var timer 	= new Timer(1000);
			var timeout = new Timer(TIEMOUT);
			
			timer.run = function()
			{
				if (_result.isDone())
				{
					timeout.stop();
					timer.stop();
					
					report();
				}
			}
			
			timeout.run = function()
			{
				timeout.stop();
				timer.stop();
				
				timeoutFail();
				report();
			}
		}
		#end
	}
	
	/**
	 * Fails all assertions remaining undone
	 */
	private function timeoutFail()
	{
		for (t in _result)
		{
			if (!t.isDone())
			{
				for (a in t.asserts())
				{
					if (!a.is_done)
					{
						t.fail(a.id, "Timeout");
					}
				}
			}
		}
	}
	
	private function getTestFields(test:Class<ITestCase<Dynamic>>):Array<String>
	{
		var out 	= [];		
		var fields 	= Type.getInstanceFields(test);
		var meta 	= Meta.getFields(test);
		
		for (fname in fields)
		{
			var fmeta = Reflect.field(meta, fname);
			
			if (fmeta != null && Reflect.hasField(fmeta, "Test"))
			{
				out.push(fname);
			}
		}
		
		return out;
	}
	
	/**
	 * Sorts the results by 
	 * @param	a
	 * @param	b
	 * @return
	 */
	private function sortResults(a:TestStatus, b:TestStatus):Int
	{
		if (a.suite == null)
			return -1;
			
		if (b.suite == null)
			return 1;
		
		var index_a = suite_order.indexOf(a.suite.name);
		var index_b = suite_order.indexOf(b.suite.name);
		
		if (index_a == index_b)
			return 0;
			
		return (index_a > index_b) ? 1 : -1;
	}
	
	private function report():Void
	{
		_result.sort(sortResults);
		
		#if !php
		trace(output_format.output(_result));
		#end
		
		#if (js || php)
		new HtmlOutput().output(result);
		#end
		
		for (cb in on_complete)
		{
			cb(this);
		}
	}
	
	@:noCompletion 
	public function get_result():TestResult
	{
		return _result;
	}
	
	public function onComplete(cb:TestRunner->Void)
	{
		on_complete.push(cb);
	}
	
}