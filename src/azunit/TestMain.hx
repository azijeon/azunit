package azunit;

import azunit.TestRunner;
import azunit.macro.SuitBuilder;

/**
 * A default main class implementation
 * 
 * @author Az
 */
class TestMain
{
	public static function main()
	{
		var suit = SuitBuilder.suit();
		
		//trace(suit);
		
		var r = new TestRunner();
		r.addSuite(suit);
		
		r.run();
	}
}