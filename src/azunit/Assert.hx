package azunit;

import azijeon.azlib.exception.Exception;
import azunit.AzUnit;
import azunit.macro.AssertMeta;
import azunit.mock.AzMock;
import haxe.Json;
import haxe.macro.Context;
import haxe.macro.Compiler;
import haxe.macro.Expr;
import haxe.PosInfos;

import azunit.TestStatus;
import azunit.macro.MacroTools;
import azijeon.azlib.macro.TypeTool;
import azijeon.azlib.tool.ValueTools;

#if macro
import haxe.macro.Type;
#end

/**
 * ...
 * @author Az
 */
class Assert
{
	#if macro
	public static inline function getStatusVar():String
	{
		//var vars 		= Context.getLocalVars();
		//var StatusClass = Reflect.field(TestStatus, "__name__").join("."); 
		//var status 		= null;
		
		return "azunit_test_result";
		
		//trace(vars);
		
		//for (name in vars.keys())
		//{
			//var v = vars.get(name);
			//
			//switch(v)
			//{
				//case TInst(t, params) : 
					//
					////trace('${t.toString()} == $StatusClass');
					//
					//if (t.toString() == StatusClass)
					//{
						//status = name;
					//}
					//
				//default: continue;
			//}
		//}
		//
		//if (status == null)
		//{
			//var test = Context.getLocalMethod();
			//throw '$test has no status var!';
		//}
		//
		//return status;
	}
	#end
	
	/**
	 * Dumps a variabel value to string 
	 * 
	 * @param	value
	 * @return
	 */
	public static function dump(value:Dynamic):String
	{
		#if !macro
		if (value == null)
			return "NULL";		
		
		var cls :Class<Dynamic> = Type.getClass(value); 
		var class_name : String = null;
		
		// Dump anonymous object
		if (cls == null && Reflect.isObject(value))
		{
			return Json.stringify(value);
		}
		
		if (cls != null)
		{
			class_name = Type.getClassName(cls);
		}
		
		if(class_name == "String")
			return Json.stringify(value);
		
		var out = new Array<String>();
		
		if (class_name != null)
		{
			out.push('($class_name)');
		}
		
		out.push(value);
		
		return out.join(" ");
		
		#else
		return "";
		#end
	}
	
	public static function getTypeName(val:Dynamic):String
	{
		#if !macro
		var t = Type.getClass(val);
		
		if (t != null)
		{
			return Type.getClassName(t);
		}
		#else
		return TypeTool.resolveTypeName(val);
		#end
		return "";
	}
	
	/**
	 * Asserts the given value is true
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function isTrue(val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("isTrue");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val : Dynamic = $val;
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				val == true,
				
				$message,
				"Expected true, but was " + Assert.dump(val) + "!");
		}
	}
	
	/**
	 * Asserts the given value is true
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function isFalse(val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("isFalse");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val : Dynamic = $val;
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				val == false,
				
				$message,
				"Expected false, but was " + Assert.dump(val) + "!");
		}
	}
	
	
	/**
	 * Asserts the given value is null
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function isNull(val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("isNull");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val = $val;
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				val == null,
				
				$message,
				"Expected NULL, but was " + Assert.dump(val) + "!");		
		}
	}
	
		/**
	 * Asserts the given value is NOT null
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function isNotNull(val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("isNotNull");
		var status 	= getStatusVar();
		
		return macro 
		{
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				$val != null,
				
				$message,
				"Expected any value, but was NULL !");
		}
	}
	
	/**
	 * Check if tested value is in a given array or iterator
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function isIn(expected:Expr, val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("isIn");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val = $val;
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				azunit.Compare.isIn($expected, val),
				
				$message,
				"Value " + Assert.dump(val) + " is not in specified range: " + Assert.dump($expected) + "!" );
		}
	}
	
	/**
	 * Check if tested value equals another
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function areEqual(expected:Expr, val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("areEqual");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val = $val;
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				azunit.Compare.equals($expected, val),
				
				$message,
				"Expected " + Assert.dump($expected) + ", but was " + Assert.dump(val) + "!");
		}
	}
	
	
	/**
	 * Check if tested value does NOT equal another
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function isBetween(min:Expr, max:Expr, val:Expr, ?not_inclusive:ExprOf<Bool>, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("isBetween");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val = $val;
			var min = $min;
			var max = $max;
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				azunit.Compare.between($min,$max, val, $not_inclusive),
				
				$message,
				"Value " + val + " is out range " + min + " - " + max);
		}
	}
	
	/**
	 * Check if tested value does NOT equal another
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function areNotEqual(expected:Expr, val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("areEqual");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val = $val;
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				!azunit.Compare.equals($expected, val),
				
				$message,
				"Expected values not to be equal, but they are!");
		}
	}
	
	/**
	 * Check if tested value equals another
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function isType(expected:ExprOf<Class<Dynamic>>, val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 			= AssertMeta.add("isType");
		var status 			= getStatusVar();
		var expected_name	= TypeTool.resolveTypeName(expected);
		
		return macro 
		{
			var val = $val;
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status },
				
				Std.isOfType(val, $expected), 
				
				$message,
				"Expected instance of " + $v{expected_name} + " but was " + Assert.dump(val) + "!");
		}
	}
	
	/**
	 * Check if tested value is not of specified type
	 * 
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function isNotType(expected:ExprOf<Class<Dynamic>>, val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("isNotType");
		var status 	= getStatusVar();
		
		return macro 
		{
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status }, 
				
				!Std.isOfType($val, $expected), 
				
				$message,
				"Expected instance of anything but" + Assert.getTypeName($expected) + "!");
		}
	}
	
	/**
	 * Asserts that an exception is thrown by the given expression
	 * 
	 * @param	val
	 * @param	expected
	 * @param	message
	 * @return
	 */
	macro public static function throws(val:Expr, ?expected:ExprOf<Class<Dynamic>>, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("throws");
		var status 	= getStatusVar();
		
		return macro 
		{
			var expected 		= $expected; 
			var s				= $i { status };
			var assert_id		= $v { info.id };	
			
			var msg:String = null;
			
			try
			{
				$val;
				
				msg = "";
				
				if (expected == null)
				{
					msg = "An exception should have been thrown!";
				}
				else
				{
					msg = "An exception of type  " + Type.getClassName(expected) + " should have been thrown.";
				}
				
				// Fail if no exception has occured
				Assert.doAssert(
					$v { info.assert }, 
					$v { info.id }, 
					$i { status }, 
					
					false,
					
					$message,
					msg
				);				
			}
			catch (e:Dynamic)
			{
				msg = "An exception " + Type.getClassName(Type.getClass(e)) + " was thrown, but was not of expected type " + Type.getClassName(expected) + ". Message: " + Std.string(e);				
				
				// Succeed if either there is no expected exception type or the type matches the caught exception
				Assert.doAssert(
					$v { info.assert }, 
					$v { info.id }, 
					$i { status }, 
					
					(expected == null || Std.isOfType(e, expected)),
					
					$message,
					msg
				);				
			}
		}
	}
	
	macro public static function count(expected:ExprOf<Int>, val:Expr, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("count");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val 		= azunit.Compare.count($val);
			var expected 	= $expected; 
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status }, 
				
				(expected == val),
				
				$message,
				'Expected count of '+ expected +' but was '+ val +' !');
		}
	}
	
	/**
	 * Assert that is always a pass - useful in certain situations
	 * @return
	 */
	macro public static function pass():Expr
	{
		var info 	= AssertMeta.add("count");
		var status 	= getStatusVar();
		
		return macro 
		{
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status }, 
				
				true,
				
				"",
				"");
		}
	}
	
	/**
	 * Assert that is always a fail - useful in certain situations
	 * @return
	 */
	macro public static function fail(message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("count");
		var status 	= getStatusVar();
		
		return macro 
		{
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status }, 
				
				false,
				
				$message,
				"This will always fail!");
		}
	}
	
	
	/**
	 * Assert that an object contains all properties and values of given expected object
	 * 
	 * @param	expected
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function map(expected:ExprOf<Dynamic>, val:ExprOf<Dynamic>, ?message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("map");
		var status 	= getStatusVar();
		
		return macro 
		{
			var val 		= azunit.Compare.count($val);
			var expected 	= $expected;
			var result		= true;
			var msg			= checkProps(expected, val);
			
			Assert.doAssert(
				$v { info.assert }, 
				$v { info.id }, 
				$i { status }, 
				
				(msg == null),
				
				$message,
				msg);
		}
	}
	
	private static function checkProps(expected: Dynamic, val:Dynamic, ?path:Array<String>):String
	{
		if (path == null)
			path = [];
		
		
		for (f in Reflect.fields(expected))
		{
			if (!Reflect.hasField(val, f))
			{
				return 'Object does not contain field "' + f + '"';				
			}
			else 
			{
				var target 	= Reflect.field(expected, f);
				var current = Reflect.field(val, f);
				
				if (current != target)
				{
					if (ValueTools.isObject(current) && ValueTools.isObject(target))
					{
						var res =  checkProps(target, current, path.concat([f]));
						
						if (res != null)
							return res;
					}
					return 'Object field "' + f + '" should have value of ' + target + ', but was ' + current;					
				}
			}				
		}
		
		return null;
	}
	
	/**
	 * Create a warning which would technically count as pass but would display a message in the test output
	 * 
	 * @param	expected
	 * @param	val
	 * @param	message
	 * @return
	 */
	macro public static function warning(message:ExprOf<String>):Expr
	{
		var info 	= AssertMeta.add("warning");
		var status 	= getStatusVar();
		
		return macro 
		{
			$i{status}.warn($v{info.id}, $message);
		}
	}
	
	@:noCompletion
	public static function doAssert(assert_name:String, assert_id:Int, s:TestStatus, expr:Bool, message:String, default_msg:String):Void
	{
		if (message == null)
		{
			message = default_msg;
		}
		
		if (expr)
		{
			s.pass(assert_id);
		}
		else
		{
			s.fail(assert_id, message);			
		}
	}
	
	
	
	
	
}