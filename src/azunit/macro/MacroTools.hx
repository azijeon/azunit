package azunit.macro;

import haxe.ds.StringMap;
import haxe.macro.Context;
import haxe.macro.Expr.ExprOf;
import haxe.macro.Expr.Position;
import haxe.macro.Expr.TypePath;
import haxe.macro.Type.Ref;
import haxe.macro.Type;

/**
 * ...
 * @author Az
 */
class MacroTools
{
	#if (macro||neko)
	
	/**
	 * Extracts the line number from given position
	 * 
	 * @param	pos
	 * @return
	 */
	public static inline function getLine(?pos:Position):Int
	{
		if (pos == null)
		{
			pos = Context.currentPos();
		}
		
		var r : EReg = ~/.hx:(\d+)/;
		
		r.match(Std.string(pos));
		
		var line : Int = Std.parseInt(r.matched(1));
		
		return line;
	}
	
	public static function fetchMeta(r:Ref<Dynamic>, key:String):Array<Int>
	{
		var out 						= [];
		var raw_meta:Array<Dynamic> 	= r.get().meta.get();
		
		for (m in raw_meta)
		{
			if (m.name == key)
			{
				out.push(m);
			}
		}
		
		return out;
	}
	
	public static function resolveTypeName(type:ExprOf<Class<Dynamic>>):String
	{
		#if macro
		// Grab passed type name
		switch(type.expr)
		{
			case EConst(c) :
				var t 		= c.getParameters()[0];
				
				return  Context.getType(t).getParameters()[0].toString();
				
			default : return "";
		}
		#else
			throw "Please use inside a macro, thank you very much!";
		#end
	}
	
	/**
	 * Create a type path from class , class name or class expression
	 * 
	 * TODO - Figure out how to set params
	 * 
	 * @param	cls
	 * @return
	 */
	public static function toTypePath(cls:Dynamic):TypePath
	{
		var cls_name : String = null;
		
		if (Std.isOfType(cls, String))
		{
			cls_name = Std.string(cls);
		}
		else if (Std.isOfType(cls, Class))
		{
			var r = new EReg("Class<([a-z.0-9_]+)>", "gi");
			r.match(Std.string(cls));
			
			cls_name = r.matched(1);
		}
		else
		{
			cls_name = resolveTypeName(cls);
		}
		
		var pack = cls_name.split(".");
		var name = pack.pop();
		
		return {
			name 	: name,
			pack 	: pack,
			params 	: [],
			sub 	: null, 
		};
	}
	
	/**
	 * Check if currently build type has own or inherited field of name
	 * @param	name
	 * @return
	 */
	public static function hasBuildField(name:String):Bool
	{
		var fields 	= Context.getBuildFields();
		var cls 	= Context.getLocalClass().get();
		
		for (f in fields)
		{
			if (f.name == name)
				return true;
		}
		
		// Check super type
		if (cls.superClass != null)
		{
			var sup = cls.superClass.t.get();
			
			while (sup != null)
			{
				for (f in sup.fields.get())
				{
					if (f.name == name)
						return true;
				}
				
				if (sup.superClass != null)
					sup = sup.superClass.t.get();
			}
		}
		
		return false;
		
	}
	#else
	
	// Not Macro
	
	
	
	#end
	

}