package azunit.macro;

import azijeon.azlib.macro.TypeTool;
import haxe.macro.Type.Ref;
import haxe.rtti.Meta;

#if macro
import haxe.macro.Context;
import haxe.macro.Compiler;
import haxe.macro.Expr;
import sys.FileSystem;
import haxe.io.Path;
#end

using StringTools;

typedef MType = haxe.macro.Type;


/**
 * ...
 * @author Az
 */
class TestBuilder
{
	#if macro
	public static function createTests(types:Array<MType>)
	{
		var classes = getApplicableClasses(types);		
	}
	
	private static function getApplicableClasses(types:Array<MType>):Array<TestBuildInfo>
	{
		var out = new Array<TestBuildInfo>();
		
		for (t in types)
		{
			switch(t)
			{
				case MType.TInst(ref, params) : 
					
					// Check da meta
					var cls  = Std.string(ref);
					var meta = MacroTools.fetchMeta(ref, ":AzTest");
					
					if (meta.length > 0)
					{
						out.push(
						{
							cls 	: cls,
							meta 	: meta,
						});
					}					
					
				default: continue;
			}
		}
		
		return out;
	}
	
	public static function createTestFor(obj_class:Class<Dynamic>):Void
	{
		
	}
	
	private static function hasTestInterface():Bool
	{
		var interfaces = Context.getLocalClass().get().interfaces;
		
		for (i in interfaces)
		{
			if (Std.string(i.t) == "azunit.ITestCase")
				return true;
		}
		
		return false;
	}
	
	macro public static function buildTest():Array<Field>
	{
		var fields = Context.getBuildFields();
		
		var RESULT_NAME = "azunit_test_result";
		
		
		if(hasTestInterface())
		{
			// Add status instance var
			// (still not sure if this is the proper way to go )
			fields.push(
			{
				name	: RESULT_NAME,
				access	: [APrivate],
				meta	: [{name : ":noCompletion", pos: Context.currentPos()}],
				kind	: FVar(null, macro null),
				pos		: Context.currentPos(),
			});	
			
			fields.push(
			{
				name	: "suite",
				access	: [APublic, AStatic],
				meta	: [],
				kind	: FVar(null, macro null),
				pos		: Context.currentPos(),
			});	
		}
		
		return fields;
	}	
	
	macro public function dumpPublicMethods(target_type:String):Void
	{
		trace("Dumping: " + target_type);
		
		var t = Context.getType(target_type);
		
		trace(t);
		
		Context.onGenerate(function(types:Array<haxe.macro.Type>)
		{
			trace("Generate: ");
			
			var out = [];
			var log = [];
			
			for (t in types)
			{
				switch(t)
				{
					case TInst(type, params) : 
						
						if (Std.string(type) == target_type)
						{
							for (field in type.get().fields.get())
							{
								if (!field.isPublic)
									continue;
								
								switch(field.kind)
								{
									case FMethod(m) : 
										
										log.push('${field.name} - ${field.type} \n');
										out.push(field.name);
										
									default: "";
								}
							}
						}						
					
					default:"";
				}
			}
			
			trace('\n\n\n<<<data>>>${out.join(",")}<<<data>>>\n\n\n');
		});
		
	}
	#end	
}

typedef TestBuildInfo =
{
	var cls 	: String;
	var meta 	: Array<Dynamic>;
}