package azunit.macro;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Expr.Field;

/**
 * ...
 * @author 
 */
class GenericBuilder 
{
	/**
	 * Remove generic type constraints while testing
	 * 
	 * @return
	 */
	public static function build():Array<Field>
	{
		var fields = Context.getBuildFields();
		
		var skip = ["haxe", "js", "cs", "php", "sys", "lua", ""];
		var cls = Context.getLocalClass();
		
		if (cls == null)
			return fields;
		
		var pack = cls.get().pack;	
			
		for (s in skip)
		{
			if (pack[0] == s)
				return fields;
		}
			
		removeConstraints(fields);
		
		return fields;
	}
	
	private static function removeConstraints(fields:Array<Field>):Void
	{
		for (f in fields)
		{
			switch (f.kind)
			{
				case FFun(fn) : if (fn.params.length > 0)
				{
					for (p in fn.params)
					{
						p.constraints = p.constraints.map(function(c)
						{
							//trace(c);
							return null;
						})
						.filter(function(v) return v != null);
					}
					//trace(fn.params);
				}
				
				default: continue;
			}
			
		}
	}
	
}