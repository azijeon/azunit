package azunit.macro;
import haxe.ds.StringMap;
import haxe.Json;

import haxe.macro.Context;
import haxe.Resource;
import haxe.Serializer;
import haxe.Unserializer;
import haxe.io.Bytes;
import haxe.crypto.Base64;

#if (macro)
import sys.FileSystem;
import haxe.macro.Compiler;
#end

/**
 * Used to gather and retrieve meta info on assertion declaration
 * 
 * @author Az
 */
class AssertMeta
{
	private static inline var 	META_RESOURCE_KEY : String = "AzijeonAssertMeta";
	
	public 	static var map : StringMap<Array<AssertInfo>>;
	private static var is_loaded : Bool = false;
	
	private static var id_seed : Int = 1;
	
	#if (macro)
	public static function add(assert:String):AssertInfo
	{
		var file 	= Context.getPosInfos(Context.currentPos()).file;
		var method 	= Context.getLocalMethod();
		var cls		= Context.getLocalClass();
		
		if (map == null)
			map = new StringMap();
		
		var key = key(Std.string(cls), Std.string(method));
		
		var array = get(key);
		
		if (array == null)
		{
			array = [];
		}
		
		var info = 
		{
			id		: id_seed++,
			assert 	: assert,
			file 	: file,
			file_abs: FileSystem.absolutePath(file),
			line	: MacroTools.getLine(),
		}
		
		array.push(info);
		map.set(key, array);
		
		return info;
	}
	
	/**
	 * Saves the generated assertion info as a resource
	 * 
	 * NOTE: Call onGenerate(), because it does not apparantly like onAfterGenerate() !
	 */
	public static function save()
	{
		if (map == null)
			return;
		
		var data : Dynamic = { };
		
		for (key in map.keys())
		{
			Reflect.setField(data, key, map.get(key));
		}
		
		var bytes = Bytes.ofString(Serializer.run(data));
		
		Context.addResource(META_RESOURCE_KEY, bytes);
	}
	
	#end
	
	public static function load()
	{
		map = new StringMap();
		var serialized : String	= Resource.getString(META_RESOURCE_KEY);
		
		if (serialized != null)
		{
			var u 		= new Unserializer(serialized);
			var data 	= u.unserialize();
			
			for (key in Reflect.fields(data))
			{
				map.set(key, Reflect.field(data, key));
			}		
		}
		
		
		is_loaded = true;
	}
	
	public inline static function key(cls, field):String
	{
		return '$cls::$field';
	}
	
	public static function get(key):Array<AssertInfo>
	{
		if (!is_loaded)
		{
			load();
		}
		
		
		var value = map.get(key);
		
		if (value == null)
			return [];
			
		return value;
	}
	
	public static function count(key:String):Int
	{
		var data = get(key);
		
		if (data == null)
			return 0;
			
		return data.length;
	}
	
	
	public static function keys():Iterator<String>
	{
		var k = map.keys();
		return k;
	}
}

typedef AssertInfo =
{
	var id		: Int;
	var assert	: String;
	var file 	: String;
	var file_abs: String;
	var line	: Null<Int>;	
}