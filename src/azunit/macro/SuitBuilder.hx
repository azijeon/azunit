package azunit.macro;
import azunit.ITestCase;
import haxe.ds.StringMap;
import haxe.io.Bytes;
import haxe.Resource;
import haxe.Serializer;
import haxe.Unserializer;

#if (macro||neko)
import azijeon.azlib.utils.haxe.SourceReflection;
import azijeon.azlib.utils.haxe.SourceReflection.RefData;
import haxe.macro.Context;
import haxe.macro.Compiler;
import haxe.macro.Expr;
import sys.FileSystem;
import haxe.io.Path;
import massive.sys.util.PathUtil;
import sys.io.File;

using haxe.macro.Tools;
#end

using StringTools;

private typedef MType = haxe.macro.Type;


/**
 * ...
 * @author Az
 */
class SuitBuilder
{
	private static var SUIT_KEY   = "AzUnitGeneratedSuitKey";
	private static var ITEST_CASE = "azunit.ITestCase";
	
	public static function __init__()
	{
		ITEST_CASE = Type.getClassName(ITestCase);		
	}
	
	#if (macro||neko)
	public static function getTestFiles(test_src:String):Array<String>
	{
		var files : Array<String> = [];
		var queue = [test_src];
		
		while (queue.length > 0)
		{
			var path:String = queue.shift();
			
			if (path != ".." && path != ".")
			{				
				var dir = FileSystem.readDirectory(path);
				
				for (f in dir)
				{
					f = Path.join([path, f]);
					
					if (FileSystem.isDirectory(f))
					{
						queue.push(f);
					}
					else if(f.endsWith(".hx"))
					{
						files.push(f);						
					}
				}
			}
		}
		
		return files;		
	}
	
	private static function getTestTypes(src:String):Array<String>
	{
		var out 	= [];
		var types 	= SourceReflection.parseDir(src);
		
		for (t in types)
		{
			if (isTestType(types, t))
			{
				out.push('${t.packageName}.${t.name}');
			}
		}
		
		return out;
	}
	
	private static function isTestType(types:Array<RefData>, type:RefData):Bool
	{
		if (type.interfaces.indexOf(ITEST_CASE) >= 0)
			return true;
			
		if (type.parentName != null)
		{
			for (t in types)
			{
				if ('${t.packageName}.${t.name}' == type.parentName)
					return isTestType(types, t);
			}
		}
		
		return false;
	}
	
	//private static function getTestTypes(test_src:String):Array<String>
	//{
		//var files 		= getTestFiles(test_src);
		////var included	= new StringMap<Bool>(); 
		//
		//var test_types : Array<String> = [];
			//
		//var t:MType 	= Context.getType("azunit.ITestCase");
		//var iTestCase 	= null;
		//
		//
		//switch(t)
		//{
			//case TInst(type, params) : iTestCase = type;
			//default: "";
		//}
		//
		//for (f in files)
		//{
			////trace('$f \n');
			//
			//var p 	= new Path(f);
			//var sep = ~/[\/\\]/gi; 
			//
			//var pack = sep.split(p.dir);
			//
			////trace("p.dir: " +p.dir );
			////trace("pack: " + pack.join("."));
			//
			//// Remove src dir from package path
			//for (item in sep.split(Config.testSrc))
			//{
				////trace('$item == ${pack[0]}');
				//
				//if (pack[0] == item)
					//pack.shift();
			//}
			//
			//var class_name 	= p.file; 
			//var class_full	= ~/^[.]/.replace('${pack.join(".")}.$class_name', "");
			//
			////trace('Class Full: $class_full');
			//
			//try
			//{
				//var t:MType = Context.getType(class_full);				
			//}
			//catch (e:Dynamic)
			//{
				//trace('Error: ${e}');
				//continue;
			//}
			//
			//switch(t)
			//{
				//case TInst(type, params) :
					//
					//var interfaces =  type.get().interfaces;
					//
					//for (i in interfaces)
					//{
						//if (i.t.toString() == iTestCase.toString())
						//{
							//test_types.push(Std.string(type));
							//trace("TestCase: " + class_full);
						//}
					//}				
					//
					//
				//default: continue;
			//}
		//}
		//
		//return test_types;
	//}
	
	/**
	 * Builds the test suit from available data
	 */
	public static function buildSuit(test_src:String)
	{
		var test_types = getTestTypes(test_src);		
		
		for (t in test_types)
		{
			// This bit forces the test to be included in compilation (for lack of a better wat)
			Context.getType(t);			
		}
		
		Context.addResource(SUIT_KEY, Bytes.ofString(Serializer.run(test_types)));
		
		var data = generateSuit(test_types);		
	}	
	
	public static function generateSuit(test_types:Array<String>):String
	{
		var buf = new StringBuf();
		
		buf.add("package;");
		buf.add("\n\n");
		buf.add("import azunit.TestSuite;\n");
		
		buf.add("\n");
		buf.add("class GeneratedTestSuit extends TestSuite\n");
		buf.add("{\n");
		buf.add("\tpublic function new(name:String ='Generated Test Suit')\n");
		buf.add("\t{\n");
		buf.add("\t\tsuper(name);\n\n");
		
		for (t in test_types)
		{
			buf.add("\t\t");
			buf.add('add($t);\n');
		}
		
		buf.add("\t}\n");
		buf.add("}\n\n");
		
		return buf.toString();		
	}
	#end
	
	public static function suit():TestSuite
	{
		var suite = new TestSuite();
		var data : String = Resource.getString(SUIT_KEY);
		
		if (data != null)
		{
			var tests : Array<Dynamic> = cast Unserializer.run(data);
			
			for (t in tests)
			{
				suite.add(cast Type.resolveClass(Std.string(t)));
			}
		}
		
		return suite;
	}
}