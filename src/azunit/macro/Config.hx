package azunit.macro;

import haxe.Json;

#if (macro || neko)
import sys.FileSystem;
import sys.io.File;

import haxe.macro.Context;
#end

/**
 * ...
 * @author Az
 */
class Config
{
	#if (macro || neko)
	static var cfg_path 	: String = "azunit.json";
	static var data 		: CFG;
	#end
	
	public static var src 		:String;
	public static var build 	:String;
	public static var testSrc	:String;
	public static var testBin	:String;
	public static var hxml		:String;
	public static var noInline	:Bool;
	public static var lib		:Array<String>;
	public static var D			:Array<String>;
	public static var macros	:Array<String>;
	public static var options	:Array<String>;
	
	public static var platforms	:Array<String>;
	
	public static var build_suit:Bool;
	
	#if (macro || neko)
	public static function load(path:String = null)
	{
		Config.build_suit = false;
		
		#if macro
		if (Context.defined("build_suit"))
		{
			Config.build_suit = true;			
		}
		#end
			
		var root = FileSystem.fullPath("./");
		
		if (path != null)
			cfg_path = path;
			
		
		if (FileSystem.exists(cfg_path))
		{
			var file = File.read(cfg_path, false);
			
			var bytes = file.readAll();
			
			file.close();
			
			try
			{
				data = Json.parse(bytes.toString());
				
				for (key in Reflect.fields(data))
				{
					Reflect.setField(Config, key, Reflect.field(data, key));
				}
			}
			catch (e:Dynamic)
			{
				#if macro
				Context.error('AzUnit config parse failed! \n' + Std.string(e), Context.currentPos());	
				#else
				throw 'AzUnit config parse failed! \n' + Std.string(e);
				#end
			}			
			
		}
		else
		{
			#if macro
			Context.error('"$cfg_path" not found in $root', Context.currentPos());
			#else
			throw '"$cfg_path" not found in $root';
			#end
		}
	}
	#end
	
	public static function isDebug()
	{
		
	}
}

typedef CFG =
{
	var src 	: String;
	var build 	: String;
	var noInline: Bool;
}

