package azunit.input;

import haxe.ds.StringMap;

#if js
import js.Browser;
#end

using StringTools;

/**
 * ...
 * @author Az
 */
class Args
{
	private static var data : StringMap<String>;
	
	public static function __init__()
	{
		data = new StringMap();
		
		#if js
		var args = ~/^[?]/.replace(js.Browser.location.search, "");
		
		for (arg in args.split("&"))
		{
			var pair 	= arg.split("=");
			var key 	= pair[0].trim();
			var value 	= null;
			
			if (pair.length == 2)
			{
			 	value = pair[1].trim();
			}
			
			data.set(key, value);
		}
		
		#else
		// Parse system args
		var args 	= Sys.args();
		data 		= new StringMap();
		
		while (args.length > 0)
		{
			var key:String = args.shift(); 
			var val:String = args.shift();
			
			if (key.startsWith("--"))
				key = key.substr(2);
			
			if (key.startsWith("-"))
				key = key.substr(1);
			
			data.set(key, val);
		}
		#end
	}
	
	public static function get(key:String):String
	{
		return data.get(key);
	}
	
	public static function keys():Iterator<String>
	{
		return data.keys();
	}
}