package azunit;

/**
 * ...
 * @author Az
 */
class TestResult
{
	var tests : Array<TestStatus>;
	
	public function new() 
	{
		tests = [];
	}
	
	public function add(s:TestStatus):Void
	{
		tests.push(s);
	}
	
	public function isDone():Bool
	{
		for (s in tests)
		{
			if (!s.isDone())
				return false;
		}
		
		return true;
	}
	
	/**
	 * Returns true if the whole result can be considered successful
	 * a) There are no failed tests
	 * b) There is at least one successful test
	 * @return
	 */
	public function isSuccess():Bool
	{
		for (t in tests)
		{
			// If there is at least one failed , it is not success
			if (t.isFail())
				return false;
		}
		
		for (t in tests)
		{
			// If there are no fails AND there is at least one success, it is success
			if (t.isPassed())
				return true;
		}
		
		// If there isn't at least one success, then all is either warnings or not tests performed, which is not a success
		return false;
	}
	
	/**
	 * Count of all successful tests
	 * @return
	 */
	public function successCount():Int
	{
		var c = 0;
		
		for (t in tests)
		{
			if (t.isDone() && t.isPassed())
			{
				c++;
			}
		}
		
		return c;
	}
	
	/**
	 * Count of all failed tests
	 * 
	 * @return
	 */
	public function failCount():Int
	{
		var c = 0;
		
		for (t in tests)
		{
			if (t.isDone() && t.isFail())
			{
				c++;
			}
		}
		
		return c;
	}
	
	/**
	 * Count of all warnings
	 * @return
	 */
	public function warnCount():Int
	{
		var c = 0;
		
		for (t in tests)
		{
			if (t.isDone() && !t.isWarning())
			{
				c++;
			}
		}
		
		return c;
	}
	
	public function doneCount():Int
	{
		var c = 0;
		
		for (t in tests)
		{
			if (t.isDone())
			{
				c++;
			}
		}
		
		return c;
	}
	
	public function allCount():Int
	{
		return tests.length;
	}
	
	public function assertsDoneCount():Int
	{
		var count = 0;
		
		for (t in tests)
		{
			for (a in t.asserts())
			{
				if (a.is_done)
				{
					count++;					
				}				
			}
		}		
		
		return count;
	}
	
	/**
	 * Counts all warning assers
	 * @return
	 */
	public function assertsWarnCount():Int
	{
		var count = 0;
		
		for (t in tests)
		{
			for (a in t.asserts())
			{
				if (a.is_done && !a.fail && !a.pass)
				{
					count++;					
				}				
			}
		}
		
		return count;
	}
	
	public function assertsAllCount():Int
	{
		var count = 0;
		
		for (t in tests)
		{
			for (a in t.asserts())
			{
				count++;
			}
		}
		
		return count;
	}
	
	public function iterator():Iterator<TestStatus>
	{
		return this.tests.iterator();
	}
	
	/**
	 * Sorts the test results by a function
	 * 
	 * @param	cb
	 */
	public function sort(cb:TestStatus->TestStatus->Int):Void
	{
		this.tests.sort(cb);
	}
}