package client;

import client.command.BuildCommand;
import haxe.ds.StringMap;
import massive.neko.cmd.CommandLineRunner;
import client.command.TestCommand;
import client.command.CreateTest;
import client.command.BuildSuitCommand;

import azunit.AzUnit;

/**
 * ...
 * @author Az
 */
class ClientApp extends CommandLineRunner
{
	public static function main() { new ClientApp(); }
	
	public function new() 
	{
		super();
		
		//mapCommand(TestCommand, 		"test", 		["t"], "This is a test command", 							"???");
		mapCommand(CreateTest, 			"create", 		["c"], "Creates a unit test for a given class", 			"???");
		mapCommand(BuildCommand, 		"build", 		["b"], "Builds a test suit from given suit config", 		"???");
		mapCommand(BuildSuitCommand, 	"build_suit", 	["s"], "Builds a test suit class from given suit config", 	"???");
		
		this.run();
	}
	
	override public function printHeader():Void 
	{
		print("");
		print("######################");
		print("Azijeon AzUnit v" + AzUnit.version());
		print("######################");
		print("");
		
		//super.printHeader();
	}
	
	
	
}