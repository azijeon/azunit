package client.command;

import haxe.io.Input;
import haxe.io.Output;
import sys.io.Process;
import massive.sys.io.FileSys;
import massive.neko.io.File;
import haxe.Json;
import azijeon.azlib.utils.haxe.HXML;
import haxe.io.Path;
import haxe.io.Eof;
import azunit.macro.Config;
import azijeon.azlib.tool.InputTool;

/**
 * ...
 * @author Az
 */
class BuildCommand extends BaseCommand
{
	var root_dir 	: File;

	public function new() 
	{
		super();
	}
	
	override public function execute():Void 
	{
		Config.build_suit = true;
		
		var suit_file = console.getNextArg();
		
		if (suit_file == "" || suit_file == null)
			suit_file = "azunit.json";
		
		setup(suit_file);
		
		var versions 	= parseHXML(Config.hxml);
		var platforms 	= Config.platforms;
		
		if (platforms == null)
			platforms = [];
		
		trace("Platforms: " + platforms);	
			
		for (v in versions)
		{
			buildSuit(v, v.target());
			
			for (p in platforms)
			{
				buildSuit(v, p);
			}
		}
	}
	
	private function setup(suit_file):Void
	{
		if (suit_file == null)
		{
			error("Suit config cannot be empty!");
		}
		
		if (FileSys.exists(suit_file))
		{
			var file = File.create(suit_file, console.dir);
			var dir  = Path.directory(file.path.toString());
			
			root_dir = File.create(dir, console.dir);
			Config.load(file.path.toString());
			
			// Check config
			if (Config.hxml == null)
				error("HXML is empty!");
			
			if (Config.testSrc == null)
				error("testSrc is not set!");
				
			if (Config.testBin == null)
				error("testBin is not set!");
		}
		else
		{
			error('Suit not found: $suit_file');
		}
		
		return;
	}
	
	private function parseHXML(hxml_path):Array<HXML>
	{
		var out 		= new Array<HXML>();
		var hxml_data 	= readFile(hxml_path);
		
		for (target in hxml_data.split("--next"))
		{
			var hxml = new azijeon.azlib.utils.haxe.HXML(target);
			out.push(hxml);
		
			print('target 	-> ${hxml.target()}');
			print('main 	-> ${hxml.main()}');
			print('lib 		-> ${hxml.lib()}');
			print("");			
		}
		
		return out;
	}
	
	private function buildSuit(hxml:HXML, target:String):Void
	{
		print('Building Suit for target: "${target}"');
		
		var bin_path = binPath("test", target);
		
		var args : Array<String> = [
			"-main",	 			"azunit.TestMain",
			'-${target}',	bin_path,	
			
			'-cp',	resolvePath(Config.testSrc),
			'-lib',	"azunit",
		];
		
		if (Config.noInline == true)
		{
			args.push("--no-inline");
		}
		
		print("\n" + args.join(" ") + "\n");
		
		// Add CP
		for (cp in hxml.cp())
		{
			args.push("-cp");
			args.push(resolvePath(cp));
		}
		
		// Add libraries
		for (lib in hxml.lib())
		{
			args.push("-lib");
			args.push(lib);
		}
		
		// Add defines
		if (hxml.D() != null)
		{
			for (d in hxml.D())
			{
				args.push("-D");
				args.push(d);
			}
		}
		
		if (Config.lib != null)
		{
			for (lib in Config.lib)
			{
				args.push("-lib");
				args.push(lib);
			}
		}
		
		// Add declarations
		if (Config.D != null)
		{
			for (d in Config.D)
			{
				args.push("-D");
				args.push(d);
			}
		}
		
		
		if (Config.options != null)
		{
			for (o in Config.options)
			{
				args.push(o);
			}
		}
		
		// Add Macros
		if (Config.macros != null)
		{
			for (m in Config.macros)
			{
				args.push("--macro");
				args.push(m);
			}
		}
		
		trace('haxe ${args.join(" ")}');
		
		var p = new Process("haxe", args);
		
		var std = InputTool.readInput(p.stdout);
		var err = InputTool.readInput(p.stderr);
		
		if (std != "")
		{
			print(std);
		}
		
		if (err == "")
		{
			print("COMPLETE");
		}
		else
		{
			print(err);
		}
		
		
	}
	

	
	private function readFile(file:String):String
	{
		var f = File.create(file, root_dir);		
		
		try
		{
			if (!f.exists)
				throw 'File not found: ${f.path}';
			
			return f.readString();			
		}
		catch (e:Dynamic)
		{
			error("readFile error: " + Std.string(e));
		}
		
		return null;
	}
	
	private function resolvePath(path:String):String
	{
		//print('Resolved: ' + path);
		
		var f = File.create(Std.string(path), root_dir);
		
		if (f.exists)
		{
			return f.path.toString();
		}
		else
		{
			throw 'Invalid path: ${f.path}';
		}		
	}
	
	private function binPath(file_name:String, target:String):String
	{
		var path = resolvePath(Config.testBin);
		
		switch(target)
		{
			case "neko" : return '$path/$file_name.n';
			case "js" 	: return '$path/$file_name.js';
			case "php" 	: return '$path/$file_name/';
			
			default: throw 'Unmapped target $target';
		}
	}
}

