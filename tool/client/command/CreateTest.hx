package client.command;
import azunit.AzUnit;
import haxe.Json;
import haxe.Resource;
import haxe.Template;
import massive.mlib.cmd.MlibCommand;
import sys.io.Process;

import sys.FileSystem;
import massive.sys.io.File;
import haxe.io.Path;

import azunit.macro.Config;

using StringTools;

/**
 * ...
 * @author Az
 */
@:require(neko)
class CreateTest extends BaseCommand
{
	var testDir : File;
	var cp 		: File;
	var lib_cp	: File;
	
	var blacklist_methods : Array<String> = ["new", "__init__"];
	
	static var test_template : String;
	
	public function new() 
	{
		super();
		
		if (test_template == null)
		{
			test_template = CompileTime.readFile("resource/TestTemplate.hx");
		}
	}
	
	override public function initialise():Void 
	{
		super.initialise();
		
		#if neko
		Config.load();
		#end
		
		if (Config.src == null)
			error("Config.src is not set!");
		
		if (Config.testSrc == null)
			error("Config.testSrc is not set!");
			
		testDir = File.create(Config.testSrc, 	console.dir);
		cp		= File.create(Config.src, 		console.dir);
		lib_cp	= File.create(console.originalDir + "/src/");
		
		if (!testDir.exists)
		{
			try
			{
				testDir.createDirectory();
			}
			catch (e:Dynamic)
			{
				error('Failed to create dir ${testDir.nativePath}');
			}
		}
	}
	
	override public function execute():Void 
	{
		var target : String = console.getOption("-for");
		
		if (target == null || target.length == 0)
			error("Invalid class file!");
		
		print("Test Src: " + Config.testSrc);	
			
		var test : String = generateTest(target);
				
		print(test);
		
		writeTest(target + "Test", test);
	}
	
	/**
	 * Creates the test file from template
	 * @param	target
	 * @return
	 */
	private function generateTest(target:String):String
	{
		var target_path		: Array<String> = target.split(".");
		var target_class	: String 		= target_path.pop();
		var target_pack		: String 		= target_path.join(".");
		var template 		: Template 		= new Template(test_template); //Resource.getString("TestTemplate"));
		
		print("Test");
		
		var data = {
			PackageName 	: target_pack,
			ClassName		: target_class,
			LibVersion		: AzUnit.version(),
			Methods			: getMethodNames(target),
		}
		
		return template.execute(data);
	}
	
	/**
	 * Writes the generated test to file
	 * 
	 * @param	path
	 * @param	testFile
	 */
	private function writeTest(path:String, testFile:String):Void
	{
		var p = Path.join('${Config.testSrc}.$path'.split(".")) + ".hx";
		var f = File.create(p, console.dir);
		
		if (!f.exists)
		{
			f.writeString(testFile);
			
			print('File Created: $p');
		}
		else
		{
			print("File already exists: " + p);
			
			var action = console.prompt("[O]verwrite , [R]ename or [C]ancel");
			
			switch(action.toUpperCase())
			{
				// Overwrite file
				case "O" : 
					
					f.writeString(testFile);
					print('File overwritten: $p');
					
				// Rename file	
				case "R" : 
					renameWrite(path, testFile);
				
				// Cancel
				default  : return; 
			}
		}
		
	}
	
	private function renameWrite(path:String, testFile:String):Void
	{
		var parts	 = path.split("."); 
		var old_name = parts.pop();
		var new_name = old_name + "1";
		
		var name = console.prompt('Enter new name [default: $new_name]');
		
		if (name != null && name.trim().length > 0)
		{
			new_name = name;
		}
		
		parts.push(new_name);
		path = parts.join(".");
		
		writeTest(path, testFile);
	}
	
	public function getMethodNames(target:String)
	{
		var file = File.create(target.replace(".", "/") + ".hx", cp);
		
		if (!file.exists)
		{
			this.error('Source file not found! - ${file.path}');
		}
		
		
		var data : String = file.readString();
		
		var method_match = new EReg("(public|static|inline|override)\\s+function\\s+([a-z_0-9]+)", "gi");
		
		var methods : Array<String> = [];
		
		method_match.map(data, function(r:EReg):String 
		{
			var method = r.matched(2);
			
			if (blacklist_methods.indexOf(method) < 0)
			{
				methods.push(method);
			}
			
			return r.matched(0);
		});
		
		return methods;
	}
}

