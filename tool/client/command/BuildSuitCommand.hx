package client.command;

import azunit.macro.SuitBuilder;
import azunit.macro.Config;
import haxe.io.Path;
import sys.io.File;
import azunit.ITestCase;
import azijeon.azlib.utils.haxe.SourceReflection;

import haxe.macro.Compiler;
using StringTools;

/**
 * ...
 * @author Аз
 */
class BuildSuitCommand extends BaseCommand
{
	override public function execute():Void 
	{
		var suit = console.getNextArg();
		loadConfig(suit);
		
		var test_src 	= Path.join([console.dir.path.toString(), Config.testSrc]);
		var files 		= getTestTypes(test_src);
		
		SourceReflection.parseDir(Config.testSrc);
		
		var suit_path = '${test_src}/GeneratedTestSuit.hx';
		var suit_file = SuitBuilder.generateSuit(files);
		
		print("Suit generated:\n");
		print(suit_file);
		File.saveContent(suit_path, suit_file);
	}
	
	private function loadConfig(suit:String)
	{
		if (suit == null)
			suit = "azunit.json";
		
		var path = Path.join([console.dir.path.toString(), suit]).toString();	
		Config.load(path);		
	}
	
	private function getTestTypes(test_src:String):Array<String>
	{
		var trim 	= new EReg('^.|.hx$', 'gi');
		
		var files 	= getTestFiles(test_src);
		
		var out 	= [];
		
		for (f in files)
		{
			//SourceReflection.parse(f);
			
			var type 	= f.replace(test_src, "").replace("\\", ".").replace("/", ".");
			type 		= trim.replace(type, "");
			
			out.push(type);
		}
		
		return out;
		
	}
	
	private function getTestFiles(test_src:String):Array<String>
	{
		var out = [];
		
		var files 		= SuitBuilder.getTestFiles(test_src);
		
		var test_case 	= Type.getClassName(ITestCase).split(".").pop();
		var pattern		= 'class\\s+([a-z_0-9]+)\\s+.*implements\\s+.*${test_case}';
		var r		 	= new EReg(pattern, 'gi');
		
		//trace(pattern);
		
		for (f in files)
		{
			var file = File.read(f, false);
			var data = file.readAll().toString();
			file.close();
			
			if (r.match(data))
			{
				out.push(f);
			}			
		}
		
		return out;
	}
}