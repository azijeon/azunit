package azunit;

import azunit.TestRunner;

import azunit.ITestCase;
import azunit.TestStatus;
import azunit.Assert;

using mockatoo.Mockatoo;

/**
 * 
 * Built with AzUnit v0.0.1
 * 
 * @author {Author}
 */
class TestRunnerTest implements ITestCase<TestRunner>
{
	private var instance : TestRunner;
	
	function setup()
	{
		instance = new TestRunner();
	}	
	
	
	@Test function addTest():Void
	{
		instance.add(TestTest);
		instance.add(TestTest);
		instance.add(TestTest);
		
		var count = 0;
		
		for (c in instance.tests())
		{
			count++;
		}
		
		Assert.areEqual(3, count);
	}
	
	
	@Test function addSuiteTest():Void
	{
		var suit = new TestSuite();
		suit.add(TestTest);
		suit.add(TestTest);
		suit.add(TestTest);
		
		instance.addSuite(suit);
		
		var count = 0;
		
		for (c in instance.tests())
		{
			count++;
		}
		
		Assert.areEqual(3, count);
	}	
	
	@Test function runTest():Void
	{
		instance.add(TestTest);
		instance.run();
		
		Assert.isTrue(instance.result.isDone());
		Assert.isTrue(instance.result.isSuccess());
		Assert.areEqual(3, instance.result.assertsAllCount());
	}
}

class TestTest implements ITestCase<Dynamic>
{
	var instance : Dynamic;
	
	@Test public function testSomething()
	{
		Assert.isTrue(true);
		Assert.isTrue(true);
		Assert.isTrue(true);
	}
}

