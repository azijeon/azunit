package azunit;

import azunit.Compare;

import azunit.ITestCase;
import azunit.TestStatus;
import azunit.Assert;

/**
 * 
 * Built with AzUnit v0.0.1
 * 
 * @author {Author}
 */
class CompareTest implements ITestCase<Compare>
{
	private var instance : Compare;
	
	@Test function testEquals(s:TestStatus):Void
	{
		// Equals Scalar
		Assert.isTrue(Compare.equals(1, 1));
		Assert.isTrue(Compare.equals("1", "1"));
		
		Assert.isFalse(Compare.equals(1,2));
		Assert.isFalse(Compare.equals("1", 1));	
		
		// Equals Object
		var a = { test: 1 , another_test : 2 };
		var b = { test: 1 , another_test : 2 };
		var c = { test: 1 , another_test : 1 };
		
		Assert.isTrue(Compare.equals(a,b));
		Assert.isTrue(Compare.equals(b, a));
		
		Assert.isFalse(Compare.equals(a,c));
		Assert.isFalse(Compare.equals(c, a));
		
		// Equals Array
		Assert.isTrue(Compare.equals([1,2,3], [1,2,3]));
		Assert.isTrue(Compare.equals([a,b,c], [a,b,c]), 	"Equals array of objects failed!");
		
		Assert.isFalse(Compare.equals([1,2,3], 	[1,2]));
		Assert.isFalse(Compare.equals([1,2,3], 	[1,3,2]));
		Assert.isFalse(Compare.equals([1,3,2], 	[1,2,3]));
		Assert.isFalse(Compare.equals([1,2], 	[1,2,3]));
		Assert.isFalse(Compare.equals([a,b,c], 	[a,c,b]));				
		Assert.isFalse(Compare.equals([a,c,b], 	[a,b,c]));
	}
}