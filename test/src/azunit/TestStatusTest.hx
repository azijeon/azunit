package azunit;

import azunit.TestStatus;
import haxe.ds.StringMap;

import azunit.ITestCase;
import azunit.TestStatus;
import azunit.Assert;
import azunit.macro.AssertMeta;
import haxe.Constraints.IMap;

using mockatoo.Mockatoo;

/**
 * 
 * Built with AzUnit v0.0.1
 * 
 * @author {Author}
 */
class TestStatusTest implements ITestCase<TestStatus>
{
	private static var original_map : StringMap<Dynamic>;
	private var instance 			: TestStatus;
	
	public static function mockAssertMeta()
	{
		original_map = Reflect.field(AssertMeta, "map");
		
		var mock_map:IMap<String, Array<AssertInfo>> = Mockatoo.mock(IMap);
		
		var a = Mockatoo.mock(AssertInfo);
		var asserts = [a];
		
		mock_map.get(cast anyString).returns(asserts);		
		
		Reflect.setField(AssertMeta, "map", mock_map);
	}
	
	public static function afterClass()
	{
		//Reflect.setField(AssertMeta, "map", original_map);
	}
	
	function setup()
	{
		instance = new TestStatus(this, "mockupMethod");
	}	
	
	
	@Test function classNameTest():Void
	{
		Assert.areEqual(Type.getClassName(Type.getClass(this)), instance.className());
	}
	
	
	@Test function assertsTest():Void
	{
		instance = new TestStatus(this, "assertsTest");
		
		Assert.isTrue(true);
		Assert.count(2, instance.asserts());
	}
	
	
	@Test function passTest():Void
	{
		var a = Mockatoo.mock(AssertStatus);
		a.id  = 1;
		
		Reflect.setField(instance, "expected_asserts", [a]);
				
		instance.pass(1);
		
		Assert.isTrue(instance.isDone());
		Assert.isTrue(instance.isPassed());
		Assert.isFalse(instance.isFail());
		
	}
	
	
	@Test function failTest():Void
	{
		var a = Mockatoo.mock(AssertStatus);
		a.id  = 1;
		
		Reflect.setField(instance, "expected_asserts", [a]);
				
		instance.fail(1, "Because so");
		
		Assert.isTrue(instance.isDone());
		Assert.isTrue(instance.isFail());
		Assert.isFalse(instance.isPassed());
		
		// Check that the test cannot be passed after it has been failed
		instance.pass(1);
		
		Assert.isTrue(instance.isDone());
		Assert.isTrue(instance.isFail());
		Assert.isFalse(instance.isPassed());
	}
}