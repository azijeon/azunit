package azunit;

import azunit.TestResult;

import azunit.ITestCase;
import azunit.TestStatus;
import azunit.Assert;
import azunit.macro.AssertMeta;

using mockatoo.Mockatoo;

/**
 * 
 * Built with AzUnit v0.0.1
 * 
 * @author {Author}
 */
class TestResultTest implements ITestCase<TestResult>
{
	private var instance : TestResult;
	
	function setup()
	{
		instance = new TestResult();
	}	
	
	
	@Test function addTest():Void
	{
		var s = Mockatoo.mock(TestStatus);
		
		instance.add(s);
		
		Assert.areEqual(1, instance.allCount());
	}
	
	
	@Test function isDoneTest():Void
	{
		var d = Mockatoo.mock(TestStatus);
		d.isDone().returns(true);
		
		instance.add(d);
		instance.add(d);
		instance.add(d);
		
		// Check success
		Assert.isTrue(instance.isDone());
		
		instance.add(d);
		
		Assert.isTrue(instance.isDone());
		
		// Check fail
		var f = Mockatoo.mock(TestStatus);
		f.isFail().returns(true);
		f.isDone().returns(true);
		f.isPassed().returns(false);
		
		instance.add(f);
		
		Assert.isFalse(instance.isSuccess());
		Assert.areEqual(1, instance.failCount());
	}
	
	
	@Test function isSuccessTest():Void
	{
		var s = Mockatoo.mock(TestStatus);
		s.isDone().returns(true);
		s.isPassed().returns(true);
		s.isFail().returns(false);
		
		instance.add(s);
		instance.add(s);
		instance.add(s);
		
		Assert.isTrue(instance.isSuccess());
		Assert.areEqual(0, instance.failCount());		
	}
	
	
	@Test function successCountTest():Void
	{
		var s = Mockatoo.mock(TestStatus);
		s.isDone().returns(true);
		s.isPassed().returns(true);
		s.isFail().returns(false);
		
		var f = Mockatoo.mock(TestStatus);
		f.isFail().returns(true);
		f.isDone().returns(true);
		f.isPassed().returns(false);
		
		instance.add(s);
		instance.add(s);
		
		instance.add(f);
		instance.add(f);
		
		instance.add(s);
		
		Assert.areEqual(3, instance.successCount());	
	}
	
	
	@Test function failCountTest():Void
	{
		var s = Mockatoo.mock(TestStatus);
		s.isDone().returns(true);
		s.isPassed().returns(true);
		s.isFail().returns(false);
		
		var f = Mockatoo.mock(TestStatus);
		f.isFail().returns(true);
		f.isDone().returns(true);
		f.isPassed().returns(false);
		
		instance.add(s);
		instance.add(s);
		
		instance.add(f);
		instance.add(f);
		
		instance.add(s);
		
		instance.add(f);
		
		Assert.areEqual(3, instance.failCount());	
	}
	
	
	@Test function doneCountTest():Void
	{
		var s = Mockatoo.mock(TestStatus);
		s.isDone().returns(true);
		s.isPassed().returns(true);
		s.isFail().returns(false);
		
		var f = Mockatoo.mock(TestStatus);
		f.isFail().returns(true);
		f.isDone().returns(true);
		f.isPassed().returns(false);
		
		instance.add(s);
		instance.add(s);
		
		instance.add(f);
		instance.add(f);
		
		instance.add(s);
		
		Assert.areEqual(5, instance.doneCount());	
	}
	
	
	@Test function allCountTest():Void
	{
		var s = Mockatoo.mock(TestStatus);
		s.isDone().returns(true);
		s.isPassed().returns(true);
		s.isFail().returns(false);
		
		var f = Mockatoo.mock(TestStatus);
		f.isFail().returns(true);
		f.isDone().returns(true);
		f.isPassed().returns(false);
		
		instance.add(s);
		instance.add(s);
		
		instance.add(f);
		instance.add(f);
		
		instance.add(s);
		
		Assert.areEqual(5, instance.allCount());	
	}
	
	
	@Test function assertsDoneCountTest():Void
	{
		var a = Mockatoo.mock(AssertStatus);
		a.is_done = true;
		
		var n = Mockatoo.mock(AssertStatus);
		n.is_done = false;
		
		var asserts = [a,a,n,a];
		
		var s = Mockatoo.mock(TestStatus);
		s.isDone().returns(true);
		s.isPassed().returns(true);
		s.isFail().returns(false);
		s.asserts().returns(asserts.iterator());
		
		instance.add(s);
		
		var count = instance.assertsDoneCount();
		Assert.areEqual(3, count);
	}
	
	
	@Test function assertsAllCountTest():Void
	{
		var a = Mockatoo.mock(AssertStatus);
		a.is_done = true;
		
		var n = Mockatoo.mock(AssertStatus);
		n.is_done = false;
		
		var asserts = [a, n, a, a];
		
		var it1 = asserts.iterator();
		var it2 = asserts.iterator();
			
		var s = Mockatoo.mock(TestStatus);
		s.isDone().returns(true);
		s.isPassed().returns(true);
		s.isFail().returns(false);
		s.asserts().returns(asserts.iterator());
		
		instance.add(s);
		
		Assert.areEqual(4, instance.assertsAllCount());
	}
	
	
	@Test function iteratorTest():Void
	{
		var s = Mockatoo.mock(TestStatus);
		
		instance.add(s);
		instance.add(s);
		instance.add(s);
		
		for (s in instance)
		{
			Assert.isType(TestStatus, s);			
		}
	}
	
		
		
	
}