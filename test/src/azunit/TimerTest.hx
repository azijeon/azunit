package azunit;

import azunit.Timer;

import azunit.ITestCase;
import azunit.TestStatus;
import azunit.Assert;

/**
 * 
 * Built with AzUnit v0.0.1
 * 
 * @author {Author}
 */
class TimerTest implements ITestCase<Timer>
{
	private var instance : Timer;
	
	function setup()
	{
		instance = new Timer();
	}	
	
	
	@Test function set_runTest():Void
	{
		instance.run = function()
		{
			Assert.isTrue(true, "The run function failed to execute!");
		}
	}
	
	
	@Test function stopTest():Void
	{
		var run_count = 0;
		
		instance.run = function()
		{
			run_count++;
		}
		
		instance.stop();
		
		// Currently there is no way to prevent the run from running at least once on a signle threaded targets
		Assert.isTrue(run_count <= 1, "The stop function failed to prevent the run from running");
	}
	
	
}