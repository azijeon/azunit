package;

import azunit.AzUnit;
import azunit.macro.SuitBuilder;
import azunit.TestRunner;

import mcover.coverage.MCoverage;
import mcover.coverage.client.TraceClient;

/**
 * ...
 * @author Az
 */
class Main
{
	public static function main()
	{
		var suit = SuitBuilder.suit();
		
		//trace(suit);
		
		var r = new TestRunner();
		r.addSuite(suit);
		
		//r.onComplete(function(r)
		//{
			//trace("DON");
			//var logger = MCoverage.getLogger();
			//logger.addClient(new TraceClient());
			//
			//logger.report();
		//});
		
		r.run();
	}
}